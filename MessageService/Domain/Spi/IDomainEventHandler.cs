using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Spi
{
    public interface IDomainEventHandler
    {
        bool Handles(IDomainEvent domainEvent);
        void Handle(IDomainEvent domainEvent);
    }
}