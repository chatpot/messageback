using System;

using MessageService.Domain.Aggregates.Users.Events;
using MessageService.Domain.Aggregates.Users.Exceptions;
using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Users
{
    public class User
    {
        private readonly IDomainEventsContainer _domainEventsContainer;

        public User(string id, IDomainEventsContainer domainEventsContainer)
        {
            Id = id;
            _domainEventsContainer = domainEventsContainer;
        }

        public string Id { get; }
        public string DisplayName { get; set; }
        public string Image { get; set; }

        public void Register()
        {
            Validate();

            _domainEventsContainer.Register(new UserRegistrationEvent(this));
        }

        public void Update(string displayName, string image)
        {
            DisplayName = displayName;
            Image = image;

            Validate();

            _domainEventsContainer.Register(new UserUpdatedEvent(this));
        }

        public void Delete()
        {
            _domainEventsContainer.Register(new UserDeletedEvent(this));
        }

        private void Validate()
        {
            if (string.IsNullOrWhiteSpace(DisplayName))
            {
                throw new DisplayNameShouldNotBeNullNorEmptyException();
            }
        }
    }
}
