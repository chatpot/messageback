using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Users.Events
{
    public class UserUpdatedEvent : IDomainEvent
    {
        public User User { get; }

        public UserUpdatedEvent(User user)
        {
            User = user;
        }
    }
}