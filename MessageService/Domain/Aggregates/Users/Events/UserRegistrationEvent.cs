using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Users.Events
{
    public class UserRegistrationEvent : IDomainEvent
    {
        public User User { get; }

        public UserRegistrationEvent(User user)
        {
            User = user;
        }
    }
}