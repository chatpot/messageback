using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Users.Events
{
    public class UserDeletedEvent : IDomainEvent
    {
        public User User { get; }

        public UserDeletedEvent(User user)
        {
            User = user;
        }
    }
}