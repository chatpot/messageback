using System.Collections.Generic;
using MessageService.Helpers;

namespace MessageService.Domain.Aggregates.Users
{
    public interface IUserRepository
    {
        IReadOnlyCollection<User> FindAll();
        User Find(string id);
        Maybe<User> FindOrDont(string id);
    }
}
