using MessageService.Domain.Api;
using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Users
{
    public interface IUserFactory
    {
        User Create(string id);
    }

    public class UserFactory : IUserFactory
    {
        private readonly IDomainEventsContainer _domainEventsContainer;

        public UserFactory(IDomainEventsContainer domainEventsContainer)
        {
            _domainEventsContainer = domainEventsContainer;
        }

        public User Create(string id)
        {
            return new User(id, _domainEventsContainer);
        }
    }
}
