using MessageService.Domain.Api.Exceptions;

namespace MessageService.Domain.Aggregates.Users.Exceptions
{
    public class DisplayNameShouldNotBeNullNorEmptyException : DomainException
    {
    }
}