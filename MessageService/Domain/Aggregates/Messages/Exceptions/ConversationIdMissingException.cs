using MessageService.Domain.Api.Exceptions;

namespace MessageService.Domain.Aggregates.Messages.Exceptions
{
    public class ConversationIdMissingException : DomainException { }
}