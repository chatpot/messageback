using MessageService.Domain.Api.Exceptions;

namespace MessageService.Domain.Aggregates.Messages.Exceptions
{
    public class NoMessageContentException : DomainException { }
}