using System;

using MessageService.Domain.Aggregates.Messages.Events;
using MessageService.Domain.Aggregates.Messages.Exceptions;
using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Messages
{
    public class Message
    {
        private readonly IDomainEventsContainer _domainEventsContainer;

        public Message(string id, IDomainEventsContainer domainEventsContainer)
        {
            Id = id;
            _domainEventsContainer = domainEventsContainer;
        }

        public string Id { get; }
        public DateTime Date { get; set; }
        public string SenderId { get; set; }
        public string ConversationId { get; set; }
        public String Content { get; set; }

        public void Send(DateTime date)
        {
            if (string.IsNullOrWhiteSpace(SenderId))
                throw new SenderIdMissingException();
            if (string.IsNullOrWhiteSpace(ConversationId))
                throw new ConversationIdMissingException();
            if (string.IsNullOrWhiteSpace(Content))
                throw new NoMessageContentException();
            Date = date;
            _domainEventsContainer.Register(new MessageSentEvent(this));
        }
    }
}
