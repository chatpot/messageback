using System;
using System.Collections.Generic;

namespace MessageService.Domain.Aggregates.Messages
{
    public interface IMessageRepository
    {
        IReadOnlyCollection<Message> FindAll();
        IReadOnlyCollection<Message> FindByConversation(string conversationId);
    }
}
