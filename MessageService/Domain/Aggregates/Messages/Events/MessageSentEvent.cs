using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Messages.Events
{
    public class MessageSentEvent : IDomainEvent
    {
        public Message Message { get; }

        public MessageSentEvent(Message message)
        {
            Message = message;
        }
    }
}