using MessageService.Domain.Api;
using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Messages
{
    public interface IMessageFactory
    {
        Message Create();
    }

    public class MessageFactory : IMessageFactory
    {
        private readonly IGuidProvider _guidProvider;
        private readonly IDomainEventsContainer _domainEventsContainer;

        public MessageFactory(IGuidProvider guidProvider, IDomainEventsContainer domainEventsContainer)
        {
            _guidProvider = guidProvider;
            _domainEventsContainer = domainEventsContainer;
        }

        public Message Create()
        {
            return new Message(_guidProvider.New(), _domainEventsContainer);
        }
    }
}