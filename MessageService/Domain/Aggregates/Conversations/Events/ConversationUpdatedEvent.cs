using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Conversations.Events {
    public class ConversationUpdatedEvent : IDomainEvent {
        public Conversation Conversation { get; }

        public ConversationUpdatedEvent (Conversation conversation) {
            Conversation = conversation;
        }
    }
}
