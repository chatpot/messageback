using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Conversations.Events {
    public class ConversationDeletedEvent : IDomainEvent {
        public Conversation Conversation { get; }

        public ConversationDeletedEvent (Conversation conversation) {
            Conversation = conversation;
        }
    }
}
