using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Conversations.Events {
    public class ConversationCreatedEvent : IDomainEvent {
        public Conversation Conversation { get; }

        public ConversationCreatedEvent (Conversation conversation) {
            Conversation = conversation;
        }
    }
}
