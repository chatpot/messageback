using MessageService.Domain.Api;
using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Conversations
{
    public interface IConversationFactory
    {
        Conversation Create();
        Conversation Create(string id);
    }

    public class ConversationFactory : IConversationFactory
    {
        private readonly IGuidProvider _guidProvider;
        private readonly IDomainEventsContainer _domainEventsContainer;

        public ConversationFactory(IGuidProvider guidProvider, IDomainEventsContainer domainEventsContainer)
        {
            _guidProvider = guidProvider;
            _domainEventsContainer = domainEventsContainer;
        }

        public Conversation Create()
        {
            return Create(_guidProvider.New());
        }

        public Conversation Create(string id)
        {
            return new Conversation(id, _domainEventsContainer);
        }

    }
}
