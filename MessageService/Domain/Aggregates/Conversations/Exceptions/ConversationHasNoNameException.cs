using MessageService.Domain.Api.Exceptions;

namespace MessageService.Domain.Aggregates.Conversations.Exceptions
{
    public class ConversationHasNoNameException : DomainException { }
}