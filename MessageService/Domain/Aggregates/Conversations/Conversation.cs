using System;
using System.Collections.Generic;
using System.Linq;

using MessageService.Domain.Aggregates.Conversations.Events;
using MessageService.Domain.Aggregates.Conversations.Exceptions;
using MessageService.Domain.Api.Events;

namespace MessageService.Domain.Aggregates.Conversations
{
    public class Conversation
    {
        private readonly IDomainEventsContainer _domainEventsContainer;

        public Conversation(string id, IDomainEventsContainer domainEventsContainer)
        {
            Id = id;
            _domainEventsContainer = domainEventsContainer;
        }

        public Conversation(string id, String name)
        {
            this.Id = id;
            this.Name = name;

        }
        public string Id { get; }
        public IEnumerable<string> Members { get; set; }
        public String Name { get; set; }
        public String Image { get; set; }

        public void Create()
        {
            if (!Members.Any())
                throw new ConversationHasNoMemberException();
            if (string.IsNullOrWhiteSpace(Name))
                throw new ConversationHasNoNameException();

            _domainEventsContainer.Register(new ConversationCreatedEvent(this));
        }

        public void Update(string name, string image, IEnumerable<string> members)
        {
            Name = name;
            Members = members;
            Image = image;

            if (string.IsNullOrWhiteSpace(Name))
                throw new ConversationHasNoNameException();

            var @event = Members.Any()
                ? new ConversationUpdatedEvent(this) as IDomainEvent
                : new ConversationDeletedEvent(this) as IDomainEvent;

            _domainEventsContainer.Register(@event);
        }
    }
}
