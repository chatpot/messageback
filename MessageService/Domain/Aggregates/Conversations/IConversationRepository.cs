using System.Collections.Generic;

namespace MessageService.Domain.Aggregates.Conversations
{
    public interface IConversationRepository
    {
        IReadOnlyCollection<Conversation> FindAll();
        IReadOnlyCollection<Conversation> FindByMemberId(string memberId);
        Conversation Find(string id);
    }
}
