using System.Collections.Generic;
using System.Linq;

namespace MessageService.Domain.Api.Events
{
    public interface IDomainEventsContainer
    {
        IReadOnlyCollection<IDomainEvent> DomainEvents { get; }

        void Register(IDomainEvent domainEvent);
        void Clear();
    }

    public class DomainEventsContainer : IDomainEventsContainer
    {
        public IReadOnlyCollection<IDomainEvent> DomainEvents => _domainEvents.ToList();

        private IList<IDomainEvent> _domainEvents;

        public DomainEventsContainer()
        {
            _domainEvents = new List<IDomainEvent>();
        }

        public void Clear()
        {
            _domainEvents.Clear();
        }

        public void Register(IDomainEvent domainEvent)
        {
            _domainEvents.Add(domainEvent);
        }
    }
}