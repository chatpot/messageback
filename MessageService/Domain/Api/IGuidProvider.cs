using System;

namespace MessageService.Domain.Api
{
    public interface IGuidProvider
    {
        string New();
    }

    public class GuidProvider : IGuidProvider
    {
        public string New()
        {
            return Guid.NewGuid().ToString();
        }
    }
}