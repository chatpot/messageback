using System;
using System.Text;

using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;

using RabbitMQ.Client;

namespace MessageService.Messaging
{
    public interface IIntegrationEventPublisher
    {
        void Publish(IIntegrationEvent message);
    }

    public interface IIntegrationEvent
    {
        string RoutingKey { get; }
        string CorrelationId { get; }
        object Content { get; }
    }

    public class IntegrationEventPublisher : IIntegrationEventPublisher, IDisposable
    {
        private const string ExchangeName = "message";
        private readonly IModel _channel;
        private readonly IConnection _connection;

        public IntegrationEventPublisher(IConfiguration configuration)
        {
            var factory = new ConnectionFactory() { HostName = (string)configuration.GetValue(typeof(string), "amqp-hostname") };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            _channel.ExchangeDeclare(ExchangeName, type: ExchangeType.Topic, true, false);
        }
        public void Dispose()
        {
            _connection.Dispose();
            _channel.Dispose();
        }

        public void Publish(IIntegrationEvent integrationEvent)
        {
            var contentAsJson = JsonConvert.SerializeObject(integrationEvent.Content);
            var messageBytes = Encoding.UTF8.GetBytes(contentAsJson);

            var props = _channel.CreateBasicProperties();
            props.CorrelationId = integrationEvent.CorrelationId.ToString();
            _channel.BasicPublish(
                exchange: ExchangeName,
                routingKey: integrationEvent.RoutingKey,
                basicProperties: props,
                body: messageBytes);
        }
    }
    public class IntegrationEvent : IIntegrationEvent
    {
        public string RoutingKey { get; }

        public string CorrelationId { get; }

        public object Content { get; }

        public IntegrationEvent(string routingKey, string correlationId, object content)
        {
            RoutingKey = routingKey;
            CorrelationId = correlationId;
            Content = content;
        }
    }
}
