using AutoMapper;

using MessageService.Domain.Aggregates.Users.Events;
using MessageService.Domain.Api;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Messaging.DomainAdapters.Models;

namespace MessageService.Messaging.DomainAdapters.EventHandlers.Users {
    public class UserDeletedEventHandler : IDomainEventHandler {
        private readonly IIntegrationEventPublisher _integrationEventPublisher;
        private readonly IGuidProvider _guidProvider;
        private readonly IMapper _mapper;

        public UserDeletedEventHandler (IIntegrationEventPublisher integrationEventPublisher, IGuidProvider guidProvider, IMapper mapper) {
            _integrationEventPublisher = integrationEventPublisher;
            _guidProvider = guidProvider;
            _mapper = mapper;
        }

        public void Handle (IDomainEvent domainEvent) {
            var userDeletedEvent = domainEvent as UserDeletedEvent;

            _integrationEventPublisher.Publish (new IntegrationEvent (
                routingKey: "deleted.user",
                correlationId : _guidProvider.New (),
                content : _mapper.Map<UserDto> (userDeletedEvent.User)
            ));
        }

        public bool Handles (IDomainEvent domainEvent) => domainEvent is UserDeletedEvent;
    }
}
