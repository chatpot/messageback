using AutoMapper;

using MessageService.Domain.Aggregates.Users.Events;
using MessageService.Domain.Api;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Messaging.DomainAdapters.Models;

namespace MessageService.Messaging.DomainAdapters.EventHandlers.Users {
    public class UserRegistrationEventHandler : IDomainEventHandler {

        private readonly IIntegrationEventPublisher _integrationEventPublisher;
        private readonly IGuidProvider _guidProvider;
        private readonly IMapper _mapper;

        public UserRegistrationEventHandler (IIntegrationEventPublisher integrationEventPublisher, IGuidProvider guidProvider, IMapper mapper) {
            _integrationEventPublisher = integrationEventPublisher;
            _guidProvider = guidProvider;
            _mapper = mapper;
        }

        public void Handle (IDomainEvent domainEvent) {
            var userRegistrationEvent = domainEvent as UserRegistrationEvent;
            _integrationEventPublisher.Publish (new IntegrationEvent (
                routingKey: "registered.user",
                correlationId : _guidProvider.New (),
                content : _mapper.Map<UserDto> (userRegistrationEvent.User)
            ));
        }

        public bool Handles (IDomainEvent domainEvent) => domainEvent is UserRegistrationEvent;
    }
}
