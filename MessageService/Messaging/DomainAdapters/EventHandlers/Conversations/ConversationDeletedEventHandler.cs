using AutoMapper;

using MessageService.Domain.Aggregates.Conversations.Events;
using MessageService.Domain.Api;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Messaging.DomainAdapters.Models;

namespace MessageService.Messaging.DomainAdapters.EventHandlers.Conversations
{
    public class ConversationDeletedEventHandler : IDomainEventHandler
    {
        private readonly IIntegrationEventPublisher _integrationEventPublisher;
        private readonly IGuidProvider _guidProvider;
        private readonly IMapper _mapper;

        public ConversationDeletedEventHandler(IIntegrationEventPublisher integrationEventPublisher, IGuidProvider guidProvider, IMapper mapper)
        {
            _integrationEventPublisher = integrationEventPublisher;
            _guidProvider = guidProvider;
            _mapper = mapper;
        }

        public void Handle(IDomainEvent domainEvent)
        {
            var conversationDeletedEvent = domainEvent as ConversationDeletedEvent;
            _integrationEventPublisher.Publish(new IntegrationEvent(
                routingKey: "deleted.conversation",
                correlationId: _guidProvider.New(),
                content: _mapper.Map<ConversationDto>(conversationDeletedEvent.Conversation)
            ));
        }

        public bool Handles(IDomainEvent domainEvent) => domainEvent is ConversationDeletedEvent;
    }
}
