using System.Linq;

using AutoMapper;

using MessageService.Domain.Aggregates.Conversations.Events;
using MessageService.Domain.Api;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Messaging.DomainAdapters.Models;

namespace MessageService.Messaging.DomainAdapters.EventHandlers.Conversations {
    public class ConversationCreatedEventHandler : IDomainEventHandler {
        private readonly IIntegrationEventPublisher _integrationEventPublisher;
        private readonly IGuidProvider _guidProvider;
        private readonly IMapper _mapper;

        public ConversationCreatedEventHandler (IIntegrationEventPublisher integrationEventPublisher, IGuidProvider guidProvider, IMapper mapper) {
            _integrationEventPublisher = integrationEventPublisher;
            _guidProvider = guidProvider;
            _mapper = mapper;
        }

        public void Handle (IDomainEvent domainEvent) {
            var conversationCreatedEvent = domainEvent as ConversationCreatedEvent;
            _integrationEventPublisher.Publish (new IntegrationEvent (
                routingKey: "created.conversation",
                correlationId : _guidProvider.New (),
                content : _mapper.Map<ConversationDto> (conversationCreatedEvent.Conversation)
            ));
        }

        public bool Handles (IDomainEvent domainEvent) => domainEvent is ConversationCreatedEvent;
    }
}
