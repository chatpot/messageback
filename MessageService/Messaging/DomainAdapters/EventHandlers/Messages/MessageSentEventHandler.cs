using AutoMapper;

using MessageService.Domain.Aggregates.Messages.Events;
using MessageService.Domain.Api;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Messaging.DomainAdapters.Models;

namespace MessageService.Messaging.DomainAdapters.EventHandlers.Messages {
    public class MessageSentEventHandler : IDomainEventHandler {

        private readonly IIntegrationEventPublisher _integrationEventPublisher;
        private readonly IGuidProvider _guidProvider;
        private readonly IMapper _mapper;

        public MessageSentEventHandler (IIntegrationEventPublisher integrationEventPublisher, IGuidProvider guidProvider, IMapper mapper) {
            _integrationEventPublisher = integrationEventPublisher;
            _guidProvider = guidProvider;
            _mapper = mapper;
        }

        public void Handle (IDomainEvent domainEvent) {
            var messageSentEvent = domainEvent as MessageSentEvent;
            _integrationEventPublisher.Publish (new IntegrationEvent (
                routingKey: "sent.message",
                correlationId : _guidProvider.New (),
                content : _mapper.Map<MessageDto> (messageSentEvent.Message)
            ));
        }

        public bool Handles (IDomainEvent domainEvent) {

            return domainEvent is MessageSentEvent;
        }
    }
}
