using System.Collections.Generic;

using Newtonsoft.Json;

namespace MessageService.Messaging.DomainAdapters.Models {
    public class ConversationDto {
        [JsonProperty ("id")]
        public string Id { get; set; }

        [JsonProperty ("name")]
        public string Name { get; set; }

        [JsonProperty ("image")]
        public string Image { get; set; }

        [JsonProperty ("members")]
        public IEnumerable<string> Members { get; set; }

    }
}
