using Newtonsoft.Json;

namespace MessageService.Messaging.DomainAdapters.Models {
    public class UserDto {
        [JsonProperty ("id")]
        public string Id { get; set; }

        [JsonProperty ("image")]
        public string Image { get; set; }

        [JsonProperty ("displayName")]
        public string DisplayName { get; set; }
    }
}
