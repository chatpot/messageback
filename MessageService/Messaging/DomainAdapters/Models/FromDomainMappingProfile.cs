using AutoMapper;

using MessageService.Domain.Aggregates.Conversations;
using MessageService.Domain.Aggregates.Messages;
using MessageService.Domain.Aggregates.Users;

namespace MessageService.Messaging.DomainAdapters.Models
{
    public class UserToUserDtoProfile : Profile
    {
        public UserToUserDtoProfile()
        {
            CreateMap<User, UserDto>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.Image, opt => opt.MapFrom(src => src.Image))
                .ForMember(dst => dst.DisplayName, opt => opt.MapFrom(src => src.DisplayName));
        }
    }

    public class ConversationToConversationDtoProfile : Profile
    {
        public ConversationToConversationDtoProfile()
        {
            CreateMap<Conversation, ConversationDto>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dst => dst.Image, opt => opt.MapFrom(src => src.Image))
                .ForMember(dst => dst.Members, opt => opt.MapFrom(src => src.Members));
        }
    }

    public class MessageToMessageDtoProfile : Profile
    {
        public MessageToMessageDtoProfile()
        {
            CreateMap<Message, MessageDto>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.ConversationId, opt => opt.MapFrom(src => src.ConversationId))
                .ForMember(dst => dst.SenderId, opt => opt.MapFrom(src => src.SenderId))
                .ForMember(dst => dst.Date, opt => opt.MapFrom(src => src.Date))
                .ForMember(dst => dst.Content, opt => opt.MapFrom(src => src.Content));
        }
    }
}
