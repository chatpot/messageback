using System;

using Newtonsoft.Json;

namespace MessageService.Messaging.DomainAdapters.Models {
    public class MessageDto {

        [JsonProperty ("id")]
        public string Id { get; set; }

        [JsonProperty ("conversationId")]
        public string ConversationId { get; set; }

        [JsonProperty ("senderId")]
        public string SenderId { get; set; }

        [JsonProperty ("content")]
        public string Content { get; set; }

        [JsonProperty ("date")]
        public DateTime Date { get; set; }
    }
}
