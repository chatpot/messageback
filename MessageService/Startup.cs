using System.Linq;
using System.Reflection;

using AutoMapper;

using MessageService.Domain.Api.Events;
using MessageService.Messaging;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using MoreLinq;

namespace MessageService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = "https://securetoken.google.com/chat-po-t";
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = "https://securetoken.google.com/chat-po-t",
                        ValidateAudience = true,
                        ValidAudience = "chat-po-t",
                        ValidateLifetime = true
                    };
                });

            services
                .AddCors(options =>
                {
                    options.AddDefaultPolicy(builder =>
                    {
                        builder
                            .AllowAnyOrigin()
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                    });
                })
                .AddAutoMapper(Assembly.GetAssembly(typeof(Startup)));

            services.AddControllers();
            typeof(Startup).Assembly.GetTypes()
                .Where(typeClass => typeClass.IsClass && typeClass.IsPublic && !typeClass.IsAbstract)
                .ForEach(typeClass =>
                {
                    typeClass
                        .GetInterfaces()
                        .Where(typeInterface => typeInterface.Assembly == typeof(Startup).Assembly && typeInterface.IsPublic
                        // TODO: find better way to do that
                        && typeInterface.Name != nameof(IDomainEvent) && typeInterface.Name != nameof(IIntegrationEvent))
                        .ForEach(typeInterface =>
                        {
                            services.AddScoped(typeInterface, typeClass);
                        });
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app
                .UseRouting()
                .UseAuthentication()
                .UseAuthorization()
                .UseCors()
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
        }
    }
}
