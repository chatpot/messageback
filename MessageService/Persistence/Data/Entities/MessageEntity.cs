using System;

namespace MessageService.Persistence.Data.Entities {
    public class MessageEntity {
        public string Id { get; set; }
        public string Content { get; set; }
        public string SenderId { get; set; }
        public string ConversationId { get; set; }
        public DateTime Date { get; set; }
    }
}
