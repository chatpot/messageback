using AutoMapper;

using MessageService.Domain.Aggregates.Conversations;
using MessageService.Domain.Aggregates.Messages;
using MessageService.Domain.Aggregates.Users;

namespace MessageService.Persistence.Data.Entities
{
    public class UserToUserEntityProfile : Profile
    {
        public UserToUserEntityProfile()
        {
            CreateMap<User, UserEntity>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.DisplayName, opt => opt.MapFrom(src => src.DisplayName))
                .ForMember(dst => dst.Image, opt => opt.MapFrom(src => src.Image));
        }
    }

    public class MessageToMessageEntityProfile : Profile
    {
        public MessageToMessageEntityProfile()
        {
            CreateMap<Message, MessageEntity>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.Content, opt => opt.MapFrom(src => src.Content))
                .ForMember(dst => dst.SenderId, opt => opt.MapFrom(src => src.SenderId))
                .ForMember(dst => dst.ConversationId, opt => opt.MapFrom(src => src.ConversationId))
                .ForMember(dst => dst.Date, opt => opt.MapFrom(src => src.Date));
        }
    }

    public class ConversationToConversationEntityProfile : Profile
    {
        public ConversationToConversationEntityProfile()
        {
            CreateMap<Conversation, ConversationEntity>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.Members, opt => opt.MapFrom(src => src.Members))
                .ForMember(dst => dst.Image, opt => opt.MapFrom(src => src.Image))
                .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name));
        }
    }
}
