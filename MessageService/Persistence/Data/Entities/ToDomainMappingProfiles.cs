using AutoMapper;

using MessageService.Domain.Aggregates.Conversations;
using MessageService.Domain.Aggregates.Messages;
using MessageService.Domain.Aggregates.Users;

namespace MessageService.Persistence.Data.Entities {
    public class UserEntityToUserProfile : Profile {
        public UserEntityToUserProfile () {
            CreateMap<UserEntity, User> ()
                .ForMember (dst => dst.Id, opt => opt.MapFrom (src => src.Id))
                .ForMember (dst => dst.DisplayName, opt => opt.MapFrom (src => src.DisplayName))
                .ForMember (dst => dst.Image, opt => opt.MapFrom (src => src.Image));
        }
    }

    public class MessageEntityToMessageProfile : Profile {
        public MessageEntityToMessageProfile () {
            CreateMap<MessageEntity, Message> ()
                .ForMember (dst => dst.Id, opt => opt.MapFrom (src => src.Id))
                .ForMember (dst => dst.Content, opt => opt.MapFrom (src => src.Content))
                .ForMember (dst => dst.SenderId, opt => opt.MapFrom (src => src.SenderId))
                .ForMember (dst => dst.ConversationId, opt => opt.MapFrom (src => src.ConversationId))
                .ForMember (dst => dst.Date, opt => opt.MapFrom (src => src.Date));
        }
    }

    public class ConversationEntityToConversationProfile : Profile {
        public ConversationEntityToConversationProfile () {
            CreateMap<ConversationEntity, Conversation> ()
                .ForMember (dst => dst.Id, opt => opt.MapFrom (src => src.Id))
                .ForMember (dst => dst.Name, opt => opt.MapFrom (src => src.Name))
                .ForMember (dst => dst.Image, opt => opt.MapFrom (src => src.Image))
                .ForMember (dst => dst.Members, opt => opt.MapFrom (src => src.Members));
        }
    }
}
