using System.Collections.Generic;

namespace MessageService.Persistence.Data.Entities {
    public class ConversationEntity {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public IEnumerable<string> Members { get; set; }
    }
}
