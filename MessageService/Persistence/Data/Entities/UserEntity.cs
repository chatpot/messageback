namespace MessageService.Persistence.Data.Entities
{
    public class UserEntity
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string Image { get; set; }
    }
}