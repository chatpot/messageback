using MessageService.Persistence.Data.Entities;

using Microsoft.Extensions.Configuration;

using MongoDB.Driver;

namespace MessageService.Persistence.Data {
    public interface IMongoDbContext {
        IMongoCollection<UserEntity> Users { get; }
        IMongoCollection<MessageEntity> Messages { get; }
        IMongoCollection<ConversationEntity> Conversations { get; }
    }

    public class MongoDbContext : IMongoDbContext {

        private readonly IMongoDatabase _database = null;

        public MongoDbContext (IConfiguration configuration) {
            var client = new MongoClient ((string) configuration.GetValue (typeof (string), "mongodb-connection-string"));
            if (client != null)
                _database = client.GetDatabase ((string) configuration.GetValue (typeof (string), "mongodb-database-name"));
        }

        public IMongoCollection<UserEntity> Users => _database.GetCollection<UserEntity> (nameof (UserEntity));
        public IMongoCollection<MessageEntity> Messages => _database.GetCollection<MessageEntity> (nameof (MessageEntity));

        public IMongoCollection<ConversationEntity> Conversations => _database.GetCollection<ConversationEntity> (nameof (ConversationEntity));
    }
}
