using AutoMapper;

using MessageService.Domain.Aggregates.Users.Events;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Persistence.Data;
using MessageService.Persistence.Data.Entities;

namespace MessageService.Persistence.DomainAdapters.EventHandlers.Users
{
    public class UserRegistrationEventHandler : IDomainEventHandler
    {
        private readonly IMongoDbContext _context;
        private readonly IMapper _mapper;

        public UserRegistrationEventHandler(IMongoDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Handle(IDomainEvent domainEvent)
        {
            var userRegistrationEvent = domainEvent as UserRegistrationEvent;
            _context.Users.InsertOne(_mapper.Map<UserEntity>(userRegistrationEvent.User));
        }

        public bool Handles(IDomainEvent domainEvent) => domainEvent is UserRegistrationEvent;
    }
}
