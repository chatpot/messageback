using AutoMapper;

using MessageService.Domain.Aggregates.Users.Events;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Persistence.Data;
using MessageService.Persistence.Data.Entities;

using MongoDB.Driver;

namespace MessageService.Persistence.DomainAdapters.EventHandlers.Users
{
    public class UserUpdatedEventHandler : IDomainEventHandler
    {
        private readonly IMongoDbContext _context;
        private readonly IMapper _mapper;

        public UserUpdatedEventHandler(IMongoDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Handle(IDomainEvent domainEvent)
        {

            var userUpdatedEvent = domainEvent as UserUpdatedEvent;
            _context.Users.FindOneAndReplace(
                c => c.Id == userUpdatedEvent.User.Id.ToString(),
                _mapper.Map<UserEntity>(userUpdatedEvent.User));
        }

        public bool Handles(IDomainEvent domainEvent) => domainEvent is UserUpdatedEvent;
    }
}
