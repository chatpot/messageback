using System.Linq;
using MessageService.Domain.Aggregates.Users.Events;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Persistence.Data;
using MessageService.Persistence.Data.Entities;
using MongoDB.Driver;

namespace MessageService.Persistence.DomainAdapters.EventHandlers.Users
{
    public class UserDeletedEventHandler : IDomainEventHandler
    {
        private readonly IMongoDbContext _context;

        public UserDeletedEventHandler(IMongoDbContext context)
        {
            _context = context;
        }

        public void Handle(IDomainEvent domainEvent)
        {
            var userDeletedEvent = domainEvent as UserDeletedEvent;
            var deletedUserId = userDeletedEvent.User.Id.ToString();

            // TODO: chain operations
            _context.Conversations.UpdateMany(
                conversation => conversation.Members.Any(member => member == deletedUserId),
                Builders<ConversationEntity>.Update.Pull(conversation => conversation.Members, deletedUserId));
            _context.Conversations
                .Find(conversation => !conversation.Members.Any())
                .ToList()
                .ForEach(conversation =>
                {
                    var conversationId = conversation.Id.ToString();
                    _context.Messages.DeleteMany(m => m.ConversationId == conversationId);
                    _context.Conversations.DeleteOne(c => c.Id == conversationId);

                });
            _context.Users.DeleteOne(user => user.Id == deletedUserId);
        }

        public bool Handles(IDomainEvent domainEvent) => domainEvent is UserDeletedEvent;
    }
}
