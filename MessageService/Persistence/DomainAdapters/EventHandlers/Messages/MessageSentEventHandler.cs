using AutoMapper;

using MessageService.Domain.Aggregates.Messages.Events;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Persistence.Data;
using MessageService.Persistence.Data.Entities;

namespace MessageService.Persistence.DomainAdapters.EventHandlers.Messages
{
    public class MessageSentEventHandler : IDomainEventHandler
    {

        private readonly IMongoDbContext _context;
        private readonly IMapper _mapper;

        public MessageSentEventHandler(IMongoDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Handle(IDomainEvent domainEvent)
        {
            var messageSentEvent = domainEvent as MessageSentEvent;
            _context.Messages.InsertOne(_mapper.Map<MessageEntity>(messageSentEvent.Message));
        }

        public bool Handles(IDomainEvent domainEvent) => domainEvent is MessageSentEvent;
    }
}
