using AutoMapper;

using MessageService.Domain.Aggregates.Conversations.Events;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Persistence.Data;

using MongoDB.Driver;

namespace MessageService.Persistence.DomainAdapters.EventHandlers.Conversations
{
    public class ConversationDeletedEventHandler : IDomainEventHandler
    {
        private readonly IMongoDbContext _context;
        private readonly IMapper _mapper;

        public ConversationDeletedEventHandler(IMongoDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Handle(IDomainEvent domainEvent)
        {
            var conversationDeletedEvent = domainEvent as ConversationDeletedEvent;

            _context.Messages.DeleteMany(m => m.ConversationId == conversationDeletedEvent.Conversation.Id.ToString());
            _context.Conversations.DeleteOne(c => c.Id == conversationDeletedEvent.Conversation.Id.ToString());

        }

        public bool Handles(IDomainEvent domainEvent) => domainEvent is ConversationDeletedEvent;
    }
}
