using AutoMapper;

using MessageService.Domain.Aggregates.Conversations.Events;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Persistence.Data;
using MessageService.Persistence.Data.Entities;

using MongoDB.Driver;

namespace MessageService.Persistence.DomainAdapters.EventHandlers.Conversations
{
    public class ConversationUpdatedEventHandler : IDomainEventHandler
    {
        private readonly IMongoDbContext _context;
        private readonly IMapper _mapper;

        public ConversationUpdatedEventHandler(IMongoDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Handle(IDomainEvent domainEvent)
        {
            var conversationUpdatedEvent = domainEvent as ConversationUpdatedEvent;
            _context.Conversations.FindOneAndReplace(
                c => c.Id == conversationUpdatedEvent.Conversation.Id.ToString(),
                _mapper.Map<ConversationEntity>(conversationUpdatedEvent.Conversation));
        }

        public bool Handles(IDomainEvent domainEvent) => domainEvent is ConversationUpdatedEvent;
    }
}
