using AutoMapper;
using MessageService.Domain.Aggregates.Conversations.Events;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MessageService.Persistence.Data;
using MessageService.Persistence.Data.Entities;

namespace MessageService.Persistence.DomainAdapters.EventHandlers.Conversations
{
    public class ConversationCreatedEventHandler : IDomainEventHandler
    {
        private readonly IMongoDbContext _context;
        private readonly IMapper _mapper;

        public ConversationCreatedEventHandler(IMongoDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Handle(IDomainEvent domainEvent)
        {
            var conversationCreatedEvent = domainEvent as ConversationCreatedEvent;
            _context.Conversations.InsertOne(_mapper.Map<ConversationEntity>(conversationCreatedEvent.Conversation));
        }

        public bool Handles(IDomainEvent domainEvent) => domainEvent is ConversationCreatedEvent;
    }
}
