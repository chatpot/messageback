using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using MessageService.Domain.Aggregates.Messages;
using MessageService.Domain.Api.Events;
using MessageService.Persistence.Data;

using MongoDB.Driver;

namespace MessageService.Persistence.DomainAdapters.Repositories
{
    public class MessageRepository : IMessageRepository
    {
        private readonly IMongoDbContext _context;
        private readonly IMapper _mapper;
        private readonly IDomainEventsContainer _domainEventsContainer;

        public MessageRepository(IMongoDbContext context, IMapper mapper, IDomainEventsContainer domainEventsContainer)
        {
            _context = context;
            _mapper = mapper;
            _domainEventsContainer = domainEventsContainer;
        }

        public IReadOnlyCollection<Message> FindAll()
        {
            return _context.Messages.Find(m => true)
                .ToList()
                .Select(messageEntity =>
                {
                    var message = new Message(messageEntity.Id, _domainEventsContainer);
                    _mapper.Map(messageEntity, message);
                    return message;
                })
                .ToList();
        }

        public IReadOnlyCollection<Message> FindByConversation(string conversationId)
        {
            return _context.Messages.Find(m => m.ConversationId == conversationId)
                .ToList()
                .Select(messageEntity =>
                {
                    var message = new Message(messageEntity.Id, _domainEventsContainer);
                    _mapper.Map(messageEntity, message);
                    return message;
                })
                .ToList();
        }
    }
}
