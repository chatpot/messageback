using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using MessageService.Domain.Aggregates.Users;
using MessageService.Domain.Api.Events;
using MessageService.Helpers;
using MessageService.Persistence.Data;
using MessageService.Persistence.Data.Entities;
using MongoDB.Driver;

namespace MessageService.Persistence.DomainAdapters.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoDbContext _context;
        private readonly IMapper _mapper;
        private readonly IDomainEventsContainer _domainEventsContainer;

        public UserRepository(IMongoDbContext context, IMapper mapper, IDomainEventsContainer domainEventsContainer)
        {
            _context = context;
            _mapper = mapper;
            _domainEventsContainer = domainEventsContainer;
        }

        public User Find(string id)
        {
            return MapUserEntity(_context.Users.Find(u => u.Id == id).Single());
        }

        public IReadOnlyCollection<User> FindAll()
        {
            return _context.Users.Find(u => true)
                .ToList()
                .Select(MapUserEntity)
                .ToList();
        }

        public Maybe<User> FindOrDont(string id)
        {
            var userEntity = _context.Users.Find(u => u.Id == id).SingleOrDefault();

            var user = (userEntity != null) ? MapUserEntity(userEntity) : null;
            return new Maybe<User>(user);
        }

        private User MapUserEntity(UserEntity userEntity)
        {
            var user = new User(userEntity.Id, _domainEventsContainer);
            _mapper.Map(userEntity, user);
            return user;
        }
    }
}
