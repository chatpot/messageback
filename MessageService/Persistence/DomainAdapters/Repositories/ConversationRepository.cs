using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using MessageService.Domain.Aggregates.Conversations;
using MessageService.Domain.Api.Events;
using MessageService.Persistence.Data;

using MongoDB.Driver;

namespace MessageService.Persistence.DomainAdapters.Repositories
{
    public class ConversationRepository : IConversationRepository
    {
        private readonly IMongoDbContext _context;
        private readonly IMapper _mapper;
        private readonly IConversationFactory _conversationFactory;

        public ConversationRepository(IMongoDbContext context, IMapper mapper, IDomainEventsContainer domainEventsContainer, IConversationFactory conversationFactory)
        {
            _context = context;
            _mapper = mapper;
            _conversationFactory = conversationFactory;
        }

        public Conversation Find(string id)
        {
            var conversationEntity = _context.Conversations.Find(c => c.Id == id).Single();
            return _mapper.Map(conversationEntity, _conversationFactory.Create(conversationEntity.Id));
        }

        public IReadOnlyCollection<Conversation> FindAll()
        {
            return _context.Conversations.Find(c => true)
                .ToList()
                .Select(conversationEntity =>
                {
                    return _mapper.Map(conversationEntity, _conversationFactory.Create(conversationEntity.Id));
                })
                .ToList();
        }

        public IReadOnlyCollection<Conversation> FindByMemberId(string memberId)
        {
            return _context.Conversations.Find(c => c.Members.Contains(memberId))
                .ToList()
                .Select(conversationEntity =>
                {
                    return _mapper.Map(conversationEntity, _conversationFactory.Create(conversationEntity.Id));
                })
                .ToList();
        }
    }
}
