using System.Collections.Generic;
using System.Linq;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using MoreLinq;

namespace MessageService.Application
{
    public interface IApplicationTransaction
    {
        void Commit();
    }

    public class ApplicationTransaction : IApplicationTransaction
    {
        private readonly IDomainEventsContainer _domainEventsContainer;
        private readonly IReadOnlyCollection<IDomainEventHandler> _domainEventsHandlers;

        public ApplicationTransaction(IDomainEventsContainer domainEventsContainer, IEnumerable<IDomainEventHandler> domainEventsHandlers)
        {
            _domainEventsContainer = domainEventsContainer;
            _domainEventsHandlers = domainEventsHandlers.ToList();
        }

        public void Commit()
        {
            _domainEventsHandlers.ForEach(eventHandler =>
            {
                _domainEventsContainer.DomainEvents
                    .Where(domainEvent => eventHandler.Handles(domainEvent))
                    .ForEach(domainEvent => eventHandler.Handle(domainEvent));
            });
            _domainEventsContainer.Clear();
        }
    }
}