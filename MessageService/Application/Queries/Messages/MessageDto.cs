using System;

namespace MessageService.Application.Queries.Messages
{
    public class MessageDto
    {
        public string Id { get; set; }
        public string SenderId { get; set; }
        public string ConversationId { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; internal set; }
    }
}
