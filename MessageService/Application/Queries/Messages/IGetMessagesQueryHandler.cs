using System.Collections.Generic;
using AutoMapper;
using MessageService.Domain.Aggregates.Messages;

namespace MessageService.Application.Queries.Messages
{
    public interface IGetMessagesQueryHandler
    {
        IReadOnlyCollection<MessageDto> Handle(GetMessagesQuery getMessagesQuery);
    }

    public class GetMessagesQuery
    {
        public string ConversationId { get; }

        public GetMessagesQuery(string conversationId)
        {
            ConversationId = conversationId;
        }

    }

    public class GetMessagesQueryHandler : IGetMessagesQueryHandler
    {
        private readonly IMapper _mapper;
        private readonly IMessageRepository _messageRepository;

        public GetMessagesQueryHandler(IMapper mapper, IMessageRepository messageRepository)
        {
            _mapper = mapper;
            _messageRepository = messageRepository;
        }

        public IReadOnlyCollection<MessageDto> Handle(GetMessagesQuery getMessagesQuery)
        {
            if (!string.IsNullOrWhiteSpace(getMessagesQuery.ConversationId))
                return _mapper.Map<IReadOnlyCollection<MessageDto>>(_messageRepository.FindByConversation(getMessagesQuery.ConversationId));
            return _mapper.Map<IReadOnlyCollection<MessageDto>>(_messageRepository.FindAll());
        }
    }
}