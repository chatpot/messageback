using System;
using System.Collections.Generic;

namespace MessageService.Application.Queries.Conversations
{
    public class ConversationDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public IEnumerable<string> Members { get; set; }
    }
}
