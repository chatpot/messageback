using AutoMapper;

using MessageService.Domain.Aggregates.Conversations;

namespace MessageService.Application.Queries.Conversations
{
    public interface IGetConversationQueryHandler
    {
        ConversationDto Handle(string id);
    }

    public class GetConversationQueryHandler : IGetConversationQueryHandler
    {
        private readonly IConversationRepository _conversationRepository;
        private readonly IMapper _mapper;

        public GetConversationQueryHandler(IConversationRepository conversationRepository, IMapper mapper)
        {
            _conversationRepository = conversationRepository;
            _mapper = mapper;
        }

        public ConversationDto Handle(string id)
        {
            return _mapper.Map<ConversationDto>(_conversationRepository.Find(id));
        }
    }
}
