using System.Collections.Generic;
using AutoMapper;
using MessageService.Domain.Aggregates.Conversations;

namespace MessageService.Application.Queries.Conversations
{
    public interface IGetConversationsQueryHandler
    {
        IReadOnlyCollection<ConversationDto> Handle(GetConversationsQuery getConversationsQuery);
    }

    public class GetConversationsQuery
    {
        public string MemberId { get; }

        public GetConversationsQuery(string memberId)
        {
            MemberId = memberId;
        }
    }

    public class GetConversationsQueryHandler : IGetConversationsQueryHandler
    {
        private readonly IConversationRepository _conversationRepository;
        private readonly IMapper _mapper;

        public GetConversationsQueryHandler(IConversationRepository conversationRepository, IMapper mapper)
        {
            _conversationRepository = conversationRepository;
            _mapper = mapper;
        }

        public IReadOnlyCollection<ConversationDto> Handle(GetConversationsQuery getConversationsQuery)
        {
            if (!string.IsNullOrWhiteSpace(getConversationsQuery.MemberId))
                return _mapper.Map<IReadOnlyCollection<ConversationDto>>(_conversationRepository.FindByMemberId(getConversationsQuery.MemberId));
            return _mapper.Map<IReadOnlyCollection<ConversationDto>>(_conversationRepository.FindAll());
        }
    }
}