using System.Collections.Generic;
using AutoMapper;
using MessageService.Domain.Aggregates.Users;

namespace MessageService.Application.Queries.Users
{
    public interface IGetUsersQueryHandler
    {
        IReadOnlyCollection<UserDto> Handle();
    }


    public class GetUsersQueryHandler : IGetUsersQueryHandler
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;

        public GetUsersQueryHandler(IMapper mapper, IUserRepository userRepository)
        {
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public IReadOnlyCollection<UserDto> Handle()
        {
            return _mapper.Map<IReadOnlyCollection<UserDto>>(_userRepository.FindAll());
        }
    }
}