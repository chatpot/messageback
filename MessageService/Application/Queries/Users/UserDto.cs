using System;

namespace MessageService.Application.Queries.Users
{
    public class UserDto
    {
        public string Id { get; set; }
        public string Image { get; set; }
        public string DisplayName { get; set; }
    }
}