using AutoMapper;
using MessageService.Domain.Aggregates.Users;

namespace MessageService.Application.Queries.Users
{
    public interface IGetUserQueryHandler
    {
        UserDto Handle(string id);
    }

    public class GetUserQueryHandler : IGetUserQueryHandler
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;

        public GetUserQueryHandler(IMapper mapper, IUserRepository userRepository)
        {
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public UserDto Handle(string id)
        {
            return _mapper.Map<UserDto>(_userRepository.Find(id));
        }
    }
}