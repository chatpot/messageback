using MessageService.Domain.Aggregates.Users;

namespace MessageService.Application.Commands.Users
{
    public interface IDeleteUserCommandHandler
    {
        void Handle(string id);
    }

    public class DeleteUserCommandHandler : IDeleteUserCommandHandler
    {
        private readonly IUserRepository _userRepository;
        private readonly IApplicationTransaction _applicationTransaction;

        public DeleteUserCommandHandler(IUserRepository userRepository, IApplicationTransaction applicationTransaction)
        {
            _userRepository = userRepository;
            _applicationTransaction = applicationTransaction;
        }

        public void Handle(string id)
        {
            var user = _userRepository.Find(id);

            user.Delete();

            _applicationTransaction.Commit();
        }
    }
}