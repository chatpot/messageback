using AutoMapper;
using MessageService.Domain.Aggregates.Users;

namespace MessageService.Application.Commands.Users
{
    public interface ICreateUserCommandHandler
    {
        string Handle(CreateUserCommand createUserCommand);
    }

    public class CreateUserCommand
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string Image { get; set; }
    }

    public class CreateUserCommandHandler : ICreateUserCommandHandler
    {
        private readonly IApplicationTransaction _applicationTransaction;
        private IMapper _mapper;
        private readonly IUserFactory _userFactory;
        private readonly IUserRepository _userRepository;

        public CreateUserCommandHandler(IApplicationTransaction applicationTransaction, IMapper mapper, IUserFactory userFactory, IUserRepository userRepository)
        {
            _applicationTransaction = applicationTransaction;
            _mapper = mapper;
            _userFactory = userFactory;
            _userRepository = userRepository;
        }

        public string Handle(CreateUserCommand createUserCommand)
        {
            var existingUser = _userRepository.FindOrDont(createUserCommand.Id);
            if (existingUser.HasValue)
            {
                throw new UserAlreadyRegisteredException();
            }

            var user = _userFactory.Create(createUserCommand.Id);

            _mapper.Map(createUserCommand, user);

            user.Register();

            _applicationTransaction.Commit();

            return user.Id;
        }
    }
}