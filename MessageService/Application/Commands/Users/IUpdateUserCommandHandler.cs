using MessageService.Domain.Aggregates.Users;

namespace MessageService.Application.Commands.Users
{
    public interface IUpdateUserCommandHandler
    {
        string Handle(UpdateUserCommand updateUserCommand);
    }

    public class UpdateUserCommand
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string Image { get; set; }
    }

    public class UpdateUserCommandHandler : IUpdateUserCommandHandler
    {
        private readonly IUserRepository _userRepository;
        private readonly IApplicationTransaction _applicationTransaction;

        public UpdateUserCommandHandler(IUserRepository userRepository, IApplicationTransaction applicationTransaction)
        {
            _userRepository = userRepository;
            _applicationTransaction = applicationTransaction;
        }

        public string Handle(UpdateUserCommand updateUserCommand)
        {
            var user = _userRepository.Find(updateUserCommand.Id);
            user.Update(updateUserCommand.DisplayName, updateUserCommand.Image);
            _applicationTransaction.Commit();
            return user.Id;
        }
    }
}