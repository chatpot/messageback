using System;

namespace MessageService.Application.Commands.Users
{
    public class UserAlreadyRegisteredException : Exception
    {
        public UserAlreadyRegisteredException()
        {
        }
    }
}