using AutoMapper;
using MessageService.Application.Commands.Conversations;
using MessageService.Application.Commands.Messages;
using MessageService.Application.Commands.Users;
using MessageService.Domain.Aggregates.Conversations;
using MessageService.Domain.Aggregates.Messages;
using MessageService.Domain.Aggregates.Users;

namespace MessageService.Application.Commands
{
    public class CreateUserCommandToUserProfile : Profile
    {
        public CreateUserCommandToUserProfile()
        {
            CreateMap<CreateUserCommand, User>()
                .ForMember(dst => dst.DisplayName, opt => opt.MapFrom(src => src.DisplayName))
                .ForMember(dst => dst.Image, opt => opt.MapFrom(src => src.Image));
        }
    }

    public class CreateMessageCommandToMessageProfile : Profile
    {
        public CreateMessageCommandToMessageProfile()
        {
            CreateMap<CreateMessageCommand, Message>()
               .ForMember(dst => dst.Content, opt => opt.MapFrom(src => src.Content))
               .ForMember(dst => dst.SenderId, opt => opt.MapFrom(src => src.SenderId))
               .ForMember(dst => dst.ConversationId, opt => opt.MapFrom(src => src.ConversationId));
        }
    }
    public class CreateConversationCommandToConversationProfile : Profile
    {
        public CreateConversationCommandToConversationProfile()
        {
            CreateMap<CreateConversationCommand, Conversation>()
               .ForMember(dst => dst.Members, opt => opt.MapFrom(src => src.Members))
               .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name));
        }
    }
}