using System;
using System.Collections.Generic;

using MessageService.Domain.Aggregates.Conversations;

namespace MessageService.Application.Commands.Conversations
{
    public interface IUpdateConversationCommandHandler
    {
        void Handle(UpdateConversationCommand updateConversationCommand);
    }

    public class UpdateConversationCommand
    {
        public IEnumerable<string> Members { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Id { get; set; }
    }

    public class UpdateConversationCommandHandler : IUpdateConversationCommandHandler
    {
        private readonly IConversationRepository _conversationRepository;
        private readonly IApplicationTransaction _applicationTransaction;

        public UpdateConversationCommandHandler(IConversationRepository conversationRepository, IApplicationTransaction applicationTransaction)
        {
            _conversationRepository = conversationRepository;
            _applicationTransaction = applicationTransaction;
        }

        public void Handle(UpdateConversationCommand updateConversationCommand)
        {
            var conversation = _conversationRepository.Find(updateConversationCommand.Id);

            conversation.Update(updateConversationCommand.Name, updateConversationCommand.Image, updateConversationCommand.Members);

            _applicationTransaction.Commit();
        }
    }
}
