using System;
using System.Collections.Generic;
using AutoMapper;
using MessageService.Domain.Aggregates.Conversations;

namespace MessageService.Application.Commands.Conversations
{
    public interface ICreateConversationCommandHandler
    {
        string Handle(CreateConversationCommand createConversationCommand);
    }

    public class CreateConversationCommand
    {
        public IEnumerable<string> Members { get; set; }
        public string Name { get; set; }
    }

    public class CreateConversationCommandHandler : ICreateConversationCommandHandler
    {
        private readonly IMapper _mapper;
        private readonly IConversationFactory _conversationFactory;
        private readonly IApplicationTransaction _applicationTransaction;

        public CreateConversationCommandHandler(IMapper mapper, IConversationFactory conversationFactory, IApplicationTransaction applicationTransaction)
        {
            _mapper = mapper;
            _conversationFactory = conversationFactory;
            _applicationTransaction = applicationTransaction;
        }

        public string Handle(CreateConversationCommand createConversationCommand)
        {
            var conversation = _conversationFactory.Create();
            _mapper.Map(createConversationCommand, conversation);

            conversation.Create();

            _applicationTransaction.Commit();
            return conversation.Id;
        }
    }
}