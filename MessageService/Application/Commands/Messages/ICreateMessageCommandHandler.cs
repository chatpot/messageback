using AutoMapper;
using MessageService.Domain.Aggregates.Messages;
using MessageService.Domain.Api;

namespace MessageService.Application.Commands.Messages
{
    public interface ICreateMessageCommandHandler
    {
        string Handle(CreateMessageCommand createMessageCommand);
    }

    public class CreateMessageCommand
    {
        public string SenderId { get; set; }
        public string ConversationId { get; set; }
        public string Content { get; set; }
    }

    public class CreateMessageCommandHandler : ICreateMessageCommandHandler
    {
        private readonly IMapper _mapper;
        private readonly IMessageFactory _messageFactory;
        private readonly IApplicationTransaction _applicationTransaction;
        private readonly IDateTimeProvider _dateTimeProvider;

        public CreateMessageCommandHandler(IMapper mapper, IMessageFactory messageFactory, IApplicationTransaction applicationTransaction, IDateTimeProvider dateTimeProvider)
        {
            _mapper = mapper;
            _messageFactory = messageFactory;
            _applicationTransaction = applicationTransaction;
            _dateTimeProvider = dateTimeProvider;
        }

        public string Handle(CreateMessageCommand createMessageCommand)
        {
            var message = _messageFactory.Create();

            _mapper.Map(createMessageCommand, message);
            message.Send(_dateTimeProvider.Now());

            _applicationTransaction.Commit();

            return message.Id;
        }
    }
}