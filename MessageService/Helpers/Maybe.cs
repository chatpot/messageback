using System;

namespace MessageService.Helpers
{
    public class Maybe<T>
    {
        private T _value;

        public Maybe(T value)
        {
            _value = value;
        }

        public bool HasValue => _value != null;

        public T Value
        {
            get
            {
                if (_value == null)
                    throw new InvalidOperationException("Trying to access null value");
                return _value;
            }
        }
    }
}