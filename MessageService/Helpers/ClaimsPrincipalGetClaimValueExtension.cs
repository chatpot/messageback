using System.Linq;
using System.Security.Claims;

namespace MessageService.Helpers
{
    public static class ClaimsPrincipalGetClaimValueExtension
    {
        public static Maybe<string> GetUserId(this ClaimsPrincipal user)
        {
            var claim = user.Claims.FirstOrDefault(c => c.Type == "user_id");

            return (claim == default(Claim))
                ? new Maybe<string>(null)
                : new Maybe<string>(claim.Value);
        }

        public static Maybe<string> GetDisplayName(this ClaimsPrincipal user)
        {
            var claim = user.Claims.FirstOrDefault(c => c.Type == "name");

            return (claim == default(Claim))
                ? new Maybe<string>(null)
                : new Maybe<string>(claim.Value);
        }

        public static Maybe<string> GetPicture(this ClaimsPrincipal user)
        {
            var claim = user.Claims.FirstOrDefault(c => c.Type == "picture");

            return (claim == default(Claim))
                ? new Maybe<string>(null)
                : new Maybe<string>(claim.Value);
        }
    }
}