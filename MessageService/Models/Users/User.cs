namespace MessageService.Models.Users
{
    public class User
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string Image { get; set; }
    }
}