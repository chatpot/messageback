namespace MessageService.Models.Users
{
    public class CreateUserRequest
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string Image { get; set; }
    }
}