using System.Collections.Generic;

namespace MessageService.Models.Conversations
{
    public class Conversation
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public IEnumerable<string> Members { get; set; }
    }
}
