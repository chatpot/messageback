using System.Collections.Generic;

namespace MessageService.Models.Conversations
{
    public class UpdateConversationRequest
    {
        public IEnumerable<string> Members { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Id { get; set; }
    }
}
