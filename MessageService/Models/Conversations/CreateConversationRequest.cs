using System.Collections.Generic;

namespace MessageService.Models.Conversations
{
    public class CreateConversationRequest
    {
        public IEnumerable<string> Members { get; set; }
        public string Name { get; set; }
    }
}
