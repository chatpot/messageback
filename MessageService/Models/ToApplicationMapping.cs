using AutoMapper;

using MessageService.Application.Commands.Conversations;
using MessageService.Application.Commands.Messages;
using MessageService.Application.Commands.Users;
using MessageService.Models.Conversations;
using MessageService.Models.Messages;
using MessageService.Models.Users;

namespace MessageService.Models
{


    public class CreateUserRequestToCreateUserCommandProfile : Profile
    {
        public CreateUserRequestToCreateUserCommandProfile()
        {
            CreateMap<CreateUserRequest, CreateUserCommand>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.DisplayName, opt => opt.MapFrom(src => src.DisplayName))
                .ForMember(dst => dst.Image, opt => opt.MapFrom(src => src.Image));
        }
    }

    public class UpdateUserRequestToUpdateUserCommandProfile : Profile
    {
        public UpdateUserRequestToUpdateUserCommandProfile()
        {
            CreateMap<UpdateUserRequest, UpdateUserCommand>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.DisplayName, opt => opt.MapFrom(src => src.DisplayName))
                .ForMember(dst => dst.Image, opt => opt.MapFrom(src => src.Image));
        }
    }


    public class CreateMessageRequestToCreateMessageCommandProfile : Profile
    {
        public CreateMessageRequestToCreateMessageCommandProfile()
        {
            CreateMap<CreateMessageRequest, CreateMessageCommand>()
                .ForMember(dst => dst.SenderId, opt => opt.MapFrom(src => src.SenderId))
                .ForMember(dst => dst.ConversationId, opt => opt.MapFrom(src => src.ConversationId))
                .ForMember(dst => dst.Content, opt => opt.MapFrom(src => src.Content));
        }
    }

    public class CreateConversationRequestToCreateConversationCommandProfile : Profile
    {
        public CreateConversationRequestToCreateConversationCommandProfile()
        {
            CreateMap<CreateConversationRequest, CreateConversationCommand>()
                .ForMember(dst => dst.Members, opt => opt.MapFrom(src => src.Members))
                .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name));
        }
    }

    public class UpdateConversationRequestToUpdateConversationCommandProfile : Profile
    {
        public UpdateConversationRequestToUpdateConversationCommandProfile()
        {
            CreateMap<UpdateConversationRequest, UpdateConversationCommand>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dst => dst.Members, opt => opt.MapFrom(src => src.Members))
                .ForMember(dst => dst.Image, opt => opt.MapFrom(src => src.Image))
                .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name));
        }
    }
}
