
namespace MessageService.Models.Messages
{
    public class CreateMessageRequest
    {
        public string SenderId { get; set; }
        public string ConversationId { get; set; }
        public string Content { get; set; }
    }
}