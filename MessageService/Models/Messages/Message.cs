using System;

namespace MessageService.Models.Messages
{
    public class Message
    {
        public string Id { get; internal set; }
        public string Content { get; internal set; }
        public string SenderId { get; internal set; }
        public string ConversationId { get; internal set; }
        public DateTime Date { get; internal set; }
    }
}
