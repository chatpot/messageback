using AutoMapper;

using MessageService.Application.Queries.Conversations;
using MessageService.Application.Queries.Messages;
using MessageService.Application.Queries.Users;
using MessageService.Models.Conversations;
using MessageService.Models.Messages;
using MessageService.Models.Users;

namespace MessageService.Models {
    public class UserDtoToUserProfile : Profile {
        public UserDtoToUserProfile () {
            CreateMap<UserDto, User> ()
                .ForMember (dst => dst.Id, opt => opt.MapFrom (src => src.Id))
                .ForMember (dst => dst.DisplayName, opt => opt.MapFrom (src => src.DisplayName))
                .ForMember (dst => dst.Image, opt => opt.MapFrom (src => src.Image));
        }
    }

    public class MessageDtoToMessageProfile : Profile {
        public MessageDtoToMessageProfile () {
            CreateMap<MessageDto, Message> ()
                .ForMember (dst => dst.Id, opt => opt.MapFrom (src => src.Id))
                .ForMember (dst => dst.Content, opt => opt.MapFrom (src => src.Content))
                .ForMember (dst => dst.SenderId, opt => opt.MapFrom (src => src.SenderId))
                .ForMember (dst => dst.ConversationId, opt => opt.MapFrom (src => src.ConversationId))
                .ForMember (dst => dst.Date, opt => opt.MapFrom (src => src.Date));
        }
    }

    public class ConversationDtoToConversationProfile : Profile {
        public ConversationDtoToConversationProfile () {
            CreateMap<ConversationDto, Conversation> ()
                .ForMember (dst => dst.Id, opt => opt.MapFrom (src => src.Id))
                .ForMember (dst => dst.Members, opt => opt.MapFrom (src => src.Members))
                .ForMember (dst => dst.Name, opt => opt.MapFrom (src => src.Name))
                .ForMember (dst => dst.Image, opt => opt.MapFrom (src => src.Image));
        }
    }
}
