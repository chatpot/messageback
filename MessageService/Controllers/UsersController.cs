using System;
using System.Collections.Generic;
using AutoMapper;

using MessageService.Application.Commands.Users;
using MessageService.Application.Queries.Users;
using MessageService.Models.Users;
using Microsoft.AspNetCore.Mvc;

namespace MessageService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUpdateUserCommandHandler _updateUserCommandHandler;
        private readonly ICreateUserCommandHandler _createUserCommandHandler;
        private readonly IDeleteUserCommandHandler _deleteUserCommandHandler;
        private readonly IGetUsersQueryHandler _getUsersQueryHandler;
        private readonly IGetUserQueryHandler _getUserQueryHandler;

        public UsersController(
            IMapper mapper,
            IUpdateUserCommandHandler updateUserCommandHandler,
            ICreateUserCommandHandler createUserCommandHandler,
            IDeleteUserCommandHandler deleteUserCommandHandler,
            IGetUsersQueryHandler getUsersQueryHandler,
            IGetUserQueryHandler getUserQueryHandler)
        {
            _mapper = mapper;
            _updateUserCommandHandler = updateUserCommandHandler;
            _createUserCommandHandler = createUserCommandHandler;
            _deleteUserCommandHandler = deleteUserCommandHandler;
            _getUsersQueryHandler = getUsersQueryHandler;
            _getUserQueryHandler = getUserQueryHandler;
        }

        [HttpGet]
        public IEnumerable<User> GetAll()
        {
            return _mapper.Map<IEnumerable<User>>(_getUsersQueryHandler.Handle());
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(string id)
        {
            try
            {
                return Ok(_mapper.Map<User>(_getUserQueryHandler.Handle(id)));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult Create(CreateUserRequest user)
        {
            return Created(string.Empty, new { id = _createUserCommandHandler.Handle(_mapper.Map<CreateUserCommand>(user)) });
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Update([FromRoute] string id, [FromBody] UpdateUserRequest user)
        {
            if (user.Id != id)
            {
                return BadRequest();
            }

            return Ok(new { id = _updateUserCommandHandler.Handle(_mapper.Map<UpdateUserCommand>(user)) });
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(string id)
        {
            try
            {
                _deleteUserCommandHandler.Handle(id);
                return Ok(new { id = id });
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}
