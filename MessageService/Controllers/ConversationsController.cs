
using System.Collections.Generic;
using AutoMapper;

using MessageService.Application.Commands.Conversations;
using MessageService.Application.Queries.Conversations;
using MessageService.Models.Conversations;
using Microsoft.AspNetCore.Mvc;

namespace MessageService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ConversationsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICreateConversationCommandHandler _createConversationCommandHandler;
        private readonly IGetConversationsQueryHandler _getConversationsQueryHandler;
        private readonly IGetConversationQueryHandler _getConversationQueryHandler;
        private readonly IUpdateConversationCommandHandler _updateConversationCommandHandler;

        public ConversationsController(
            IMapper mapper,
            ICreateConversationCommandHandler createConversationCommandHandler,
            IGetConversationsQueryHandler getConversationsQueryHandler,
            IGetConversationQueryHandler getConversationQueryHandler,
            IUpdateConversationCommandHandler updateConversationCommandHandler)
        {
            _mapper = mapper;
            _createConversationCommandHandler = createConversationCommandHandler;
            _getConversationsQueryHandler = getConversationsQueryHandler;
            _getConversationQueryHandler = getConversationQueryHandler;
            _updateConversationCommandHandler = updateConversationCommandHandler;
        }

        [HttpPost]
        public IActionResult Create(CreateConversationRequest conversationRequest)
        {
            return Created(string.Empty, new
            {
                id = _createConversationCommandHandler.Handle(_mapper.Map<CreateConversationCommand>(conversationRequest))
            });
        }

        [HttpGet]
        [Route("{id}")]
        public Conversation Get(string id)
        {
            return _mapper.Map<Conversation>(_getConversationQueryHandler.Handle(id));
        }

        [HttpGet]
        public IEnumerable<Conversation> GetByMemberId(string memberId)
        {
            return _mapper.Map<IEnumerable<Conversation>>(
                _getConversationsQueryHandler.Handle(new GetConversationsQuery(memberId))
            );
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Put(string id, UpdateConversationRequest updateConversationRequest)
        {
            if (id != updateConversationRequest.Id)
            {
                return BadRequest();
            }

            _updateConversationCommandHandler.Handle(_mapper.Map<UpdateConversationCommand>(updateConversationRequest));
            return NoContent();
        }
    }
}
