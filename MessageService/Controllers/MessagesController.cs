using System.Collections.Generic;
using MessageService.Application.Commands.Messages;
using MessageService.Models.Messages;
using MessageService.Application.Queries.Messages;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;

namespace MessageService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessagesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICreateMessageCommandHandler _createMessageCommandHandler;
        private readonly IGetMessagesQueryHandler _getMessagesQueryHandler;

        public MessagesController(IMapper mapper, ICreateMessageCommandHandler createMessageCommandHandler,
            IGetMessagesQueryHandler getMessagesQueryHandler)
        {
            _mapper = mapper;
            _createMessageCommandHandler = createMessageCommandHandler;
            _getMessagesQueryHandler = getMessagesQueryHandler;
        }

        [HttpPost]
        public IActionResult Create(CreateMessageRequest createMessageRequest)
        {
            return Created(string.Empty, new
            {
                id = _createMessageCommandHandler.Handle(_mapper.Map<CreateMessageCommand>(createMessageRequest))
            });
        }

        [HttpGet]
        public IEnumerable<Message> GetByConversationId(string conversationId)
        {
            return _mapper.Map<IEnumerable<Message>>(
                _getMessagesQueryHandler.Handle(new GetMessagesQuery(conversationId))
            );
        }
    }
}
