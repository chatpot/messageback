FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env

WORKDIR /usr/src/app

COPY . .

RUN dotnet test
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1

WORKDIR /usr/src/app

COPY --from=build-env /usr/src/app/out .

EXPOSE 80

CMD ["/usr/src/app/MessageService"]
