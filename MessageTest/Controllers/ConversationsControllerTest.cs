using System.Collections.Generic;
using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using MessageService.Application.Commands.Conversations;
using MessageService.Application.Queries.Conversations;
using MessageService.Controllers;
using MessageService.Models.Conversations;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace MessageTest.Controllers
{
    public class ConversationsControllerTest
    {
        private ConversationsController MakeSut(
            IMapper mapper = null,
            ICreateConversationCommandHandler createConversationCommandHandler = null,
            IGetConversationsQueryHandler getConversationsQueryHandler = null,
            IGetConversationQueryHandler getConversationQueryHandler = null,
            IUpdateConversationCommandHandler updateConversationCommandHandler = null)
        {
            return new ConversationsController(
                mapper ?? Mock.Of<IMapper>(),
                createConversationCommandHandler ?? Mock.Of<ICreateConversationCommandHandler>(),
                getConversationsQueryHandler ?? Mock.Of<IGetConversationsQueryHandler>(),
                getConversationQueryHandler ?? Mock.Of<IGetConversationQueryHandler>(),
                updateConversationCommandHandler ?? Mock.Of<IUpdateConversationCommandHandler>());
        }

        [Theory, AutoMoq]
        public void Create_Should_MapRequestIntoCommandAndCallCommandHandler(
            [Frozen] Mock<IMapper> mapperMock,
            [Frozen] Mock<ICreateConversationCommandHandler> createConversationCommandHandlerMock,
            CreateConversationRequest given,
            CreateConversationCommand createConversationCommand,
            string expectedId
        )
        {
            // Arrange
            mapperMock.Setup(m => m.Map<CreateConversationCommand>(given)).Returns(createConversationCommand);
            createConversationCommandHandlerMock.Setup(m => m.Handle(createConversationCommand)).Returns(expectedId);

            var sut = MakeSut(
                mapper: mapperMock.Object,
                createConversationCommandHandler: createConversationCommandHandlerMock.Object);

            // Act
            var actual = sut.Create(given);

            // Assert
            actual.Should().BeOfType<CreatedResult>();
            var typedActual = actual as CreatedResult;
            typedActual.Location.Should().BeEmpty();
            typedActual.Value.Should().BeEquivalentTo(new { id = expectedId });

            mapperMock.VerifyAll();
            createConversationCommandHandlerMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void Get_ShouldReturnMatchingConversation(
            [Frozen] Mock<IMapper> mapperMock,
            [Frozen] Mock<IGetConversationQueryHandler> getConversationQueryHandlerMock,
            string given,
            ConversationDto conversationDto,
            Conversation expected)
        {
            // Arrange
            getConversationQueryHandlerMock
                .Setup(m => m.Handle(given))
                .Returns(conversationDto);

            mapperMock
                .Setup(m => m.Map<Conversation>(conversationDto))
                .Returns(expected);

            var sut = MakeSut(mapper: mapperMock.Object, getConversationQueryHandler: getConversationQueryHandlerMock.Object);

            // Act
            var actual = sut.Get(given);

            // Assert
            actual.Should().BeEquivalentTo(expected);

            getConversationQueryHandlerMock.VerifyAll();
            mapperMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void GetByMemberId_ShouldReturnMatchingConversations(
            [Frozen] Mock<IMapper> mapperMock,
            [Frozen] Mock<IGetConversationsQueryHandler> getConversationsQueryHandlerMock,
            string given,
            IReadOnlyCollection<ConversationDto> conversationsDto,
            IEnumerable<Conversation> expected)
        {
            // Arrange
            getConversationsQueryHandlerMock
                .Setup(m => m.Handle(It.Is<GetConversationsQuery>(query => query.MemberId == given)))
                .Returns(conversationsDto);

            mapperMock
                .Setup(m => m.Map<IEnumerable<Conversation>>(conversationsDto))
                .Returns(expected);

            var sut = MakeSut(mapper: mapperMock.Object, getConversationsQueryHandler: getConversationsQueryHandlerMock.Object);

            // Act
            var actual = sut.GetByMemberId(given);

            // Assert
            actual.Should().BeEquivalentTo(expected);

            getConversationsQueryHandlerMock.VerifyAll();
            mapperMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void Put_WhenIdMatches_ShouldReturnNoContentAfterUpdate(
            [Frozen] Mock<IUpdateConversationCommandHandler> updateConversationCommandHandlerMock,
            [Frozen] Mock<IMapper> mapperMock,
            UpdateConversationRequest given,
            UpdateConversationCommand command
        )
        {
            // Arrange
            mapperMock
                .Setup(m => m.Map<UpdateConversationCommand>(given))
                .Returns(command);
            updateConversationCommandHandlerMock.Setup(m => m.Handle(command));

            var sut = MakeSut(
                mapper: mapperMock.Object,
                updateConversationCommandHandler: updateConversationCommandHandlerMock.Object);

            // Act
            var actual = sut.Put(given.Id, given);

            // Assert
            actual.Should().BeOfType<NoContentResult>();

            mapperMock.VerifyAll();
            updateConversationCommandHandlerMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void Put_WhenIdDoesNotMatch_ShouldReturnBadRequest(
            [Frozen] Mock<IUpdateConversationCommandHandler> updateConversationCommandHandlerMock,
            [Frozen] Mock<IMapper> mapperMock,
            UpdateConversationRequest givenRequest,
            string givenId
        )
        {

            // Arrange
            var sut = MakeSut(
                mapper: mapperMock.Object,
                updateConversationCommandHandler: updateConversationCommandHandlerMock.Object);

            // Act
            var actual = sut.Put(givenId, givenRequest);

            // Assert
            actual.Should().BeOfType<BadRequestResult>();

            mapperMock.Verify(m => m.Map<UpdateConversationCommand>(It.IsAny<UpdateConversationRequest>()), Times.Never);
            updateConversationCommandHandlerMock.Verify(m => m.Handle(It.IsAny<UpdateConversationCommand>()), Times.Never);
        }
    }
}