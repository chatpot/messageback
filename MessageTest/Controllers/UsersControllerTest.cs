using System;
using System.Collections.Generic;
using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using MessageService.Application.Commands.Users;
using MessageService.Application.Queries.Users;
using MessageService.Controllers;
using MessageService.Models.Users;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace MessageTest.Controllers
{
    public class UsersControllerTest
    {
        private UsersController MakeSut(
            IMapper mapper = null,
            IUpdateUserCommandHandler updateUserCommandHandler = null,
            ICreateUserCommandHandler createUserCommandHandler = null,
            IDeleteUserCommandHandler deleteUserCommandHandler = null,
            IGetUsersQueryHandler getUsersQueryHandler = null,
            IGetUserQueryHandler getUserQueryHandler = null)
        {
            return new UsersController(
                mapper: mapper,
                updateUserCommandHandler: updateUserCommandHandler,
                createUserCommandHandler: createUserCommandHandler,
                deleteUserCommandHandler: deleteUserCommandHandler,
                getUsersQueryHandler: getUsersQueryHandler,
                getUserQueryHandler: getUserQueryHandler);
        }

        [Theory, AutoMoq]
        public void GetAll(
            [Frozen] Mock<IGetUsersQueryHandler> getUsersQueryHandlerMock,
            [Frozen]Mock<IMapper> mapperMock,
            IReadOnlyCollection<UserDto> usersDto,
            IEnumerable<User> expected)
        {
            // Arrange
            getUsersQueryHandlerMock.Setup(m => m.Handle()).Returns(usersDto);
            mapperMock.Setup(m => m.Map<IEnumerable<User>>(usersDto)).Returns(expected);

            var sut = MakeSut(mapper: mapperMock.Object, getUsersQueryHandler: getUsersQueryHandlerMock.Object);

            // Act
            var actual = sut.GetAll();

            // Assert
            actual.Should().BeEquivalentTo(expected);
            getUsersQueryHandlerMock.VerifyAll();
            mapperMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void Get_WhenNoException_ShouldReturnOk(
                    [Frozen] Mock<IGetUserQueryHandler> getUserQueryHandlerMock,
                    [Frozen] Mock<IMapper> mapperMock,
                    string given,
                    UserDto user,
                    User expected
                )
        {
            // Arrange
            getUserQueryHandlerMock.Setup(m => m.Handle(given)).Returns(user);
            mapperMock.Setup(m => m.Map<User>(user)).Returns(expected);

            var sut = MakeSut(mapper: mapperMock.Object, getUserQueryHandler: getUserQueryHandlerMock.Object);

            // Act
            var actual = sut.Get(given);

            // Assert
            actual.Should().BeOfType<OkObjectResult>();
            (actual as OkObjectResult).Value.Should().Be(expected);
            getUserQueryHandlerMock.VerifyAll();
            mapperMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void Get_WhenException_ShouldReturnNotFound(
                    [Frozen] Mock<IGetUserQueryHandler> getUserQueryHandlerMock,
                    [Frozen] Mock<IMapper> mapperMock,
                    string given,
                    UserDto user,
                    User expected
                )
        {
            // Arrange
            getUserQueryHandlerMock.Setup(m => m.Handle(given)).Throws(new Exception());

            var sut = MakeSut(mapper: mapperMock.Object, getUserQueryHandler: getUserQueryHandlerMock.Object);

            // Act
            var actual = sut.Get(given);

            // Assert
            actual.Should().BeOfType<NotFoundResult>();
            getUserQueryHandlerMock.VerifyAll();
            mapperMock.Verify(m => m.Map<User>(user), Times.Never);
        }

        [Theory, AutoMoq]
        public void Create(
            [Frozen] Mock<ICreateUserCommandHandler> createUserCommandHandlerMock,
            [Frozen] Mock<IMapper> mapperMock,
            CreateUserRequest given,
            CreateUserCommand command,
            string expectedId

        )
        {
            // Arrange
            mapperMock.Setup(m => m.Map<CreateUserCommand>(given)).Returns(command);
            createUserCommandHandlerMock.Setup(m => m.Handle(command)).Returns(expectedId);
            var sut = MakeSut(mapper: mapperMock.Object, createUserCommandHandler: createUserCommandHandlerMock.Object);

            // Act
            var actual = sut.Create(given);

            // Assert
            actual.Should().BeOfType<CreatedResult>();
            (actual as CreatedResult).Location.Should().BeEmpty();
            (actual as CreatedResult).Value.Should().BeEquivalentTo(new { id = expectedId });

            mapperMock.VerifyAll();
            createUserCommandHandlerMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void Update_WhenIdsMatch_ShouldUpdateUser(
            [Frozen] Mock<IUpdateUserCommandHandler> updateUserCommandHandlerMock,
            [Frozen] Mock<IMapper> mapperMock,
            UpdateUserRequest given,
            UpdateUserCommand command,
            string expectedId
        )
        {
            // Arrange
            updateUserCommandHandlerMock.Setup(m => m.Handle(command)).Returns(expectedId);
            mapperMock.Setup(m => m.Map<UpdateUserCommand>(given)).Returns(command);
            var sut = MakeSut(mapper: mapperMock.Object, updateUserCommandHandler: updateUserCommandHandlerMock.Object);

            // Act
            var actual = sut.Update(given.Id, given);

            // Assert
            actual.Should().BeOfType<OkObjectResult>();
            (actual as OkObjectResult).Value.Should().BeEquivalentTo(new { id = expectedId });
            updateUserCommandHandlerMock.VerifyAll();
            mapperMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void Update_WhenIdsMisMatch_ShouldReturnBadRequest(
            [Frozen] Mock<IUpdateUserCommandHandler> updateUserCommandHandlerMock,
            [Frozen] Mock<IMapper> mapperMock,
            UpdateUserRequest given,
            string mismatchingId
        )
        {
            // Arrange
            var sut = MakeSut(mapper: mapperMock.Object, updateUserCommandHandler: updateUserCommandHandlerMock.Object);

            // Act
            var actual = sut.Update(mismatchingId, given);

            // Assert
            actual.Should().BeOfType<BadRequestResult>();

            updateUserCommandHandlerMock.Verify(m => m.Handle(It.IsAny<UpdateUserCommand>()), Times.Never);
            mapperMock.Verify(m => m.Map<UpdateUserRequest>(It.IsAny<UpdateUserRequest>()), Times.Never);

        }

        [Theory, AutoMoq]
        public void Delete_WhenNoException_ShouldReturnDeleted(
            [Frozen] Mock<IDeleteUserCommandHandler> deleteUserCommandHandlerMock,
            string given
        )
        {
            // Arrange
            deleteUserCommandHandlerMock.Setup(m => m.Handle(given));
            var sut = MakeSut(deleteUserCommandHandler: deleteUserCommandHandlerMock.Object);

            // Act
            var actual = sut.Delete(given);

            // Assert
            actual.Should().BeOfType<OkObjectResult>();
            (actual as OkObjectResult).Value.Should().BeEquivalentTo(new { id = given });

            deleteUserCommandHandlerMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void Delete_WhenException_ShouldReturnNotFound(
            [Frozen] Mock<IDeleteUserCommandHandler> deleteUserCommandHandlerMock,
            string given
        )
        {
            // Arrange
            deleteUserCommandHandlerMock.Setup(m => m.Handle(given)).Throws(new Exception());
            var sut = MakeSut(deleteUserCommandHandler: deleteUserCommandHandlerMock.Object);

            // Act
            var actual = sut.Delete(given);

            // Assert
            actual.Should().BeOfType<NotFoundResult>();

            deleteUserCommandHandlerMock.VerifyAll();
        }
    }
}