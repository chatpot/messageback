using System.Collections.Generic;
using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using MessageService.Application.Commands.Messages;
using MessageService.Application.Queries.Messages;
using MessageService.Controllers;
using MessageService.Models.Messages;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace MessageTest.Controllers
{
    public class MessagesControllerTest
    {
        private MessagesController MakeSut(
            IMapper mapper = null,
            ICreateMessageCommandHandler createMessageCommandHandler = null,
            IGetMessagesQueryHandler getMessagesQueryHandler = null)
        {
            return new MessagesController(
                mapper: mapper ?? Mock.Of<IMapper>(),
                createMessageCommandHandler: createMessageCommandHandler ?? Mock.Of<ICreateMessageCommandHandler>(),
                getMessagesQueryHandler: getMessagesQueryHandler ?? Mock.Of<IGetMessagesQueryHandler>()
            );
        }

        [Theory, AutoMoq]
        public void Create([Frozen] Mock<ICreateMessageCommandHandler> createMessageCommandHandlerMock,
        [Frozen] Mock<IMapper> mapperMock,
        CreateMessageRequest given,
        CreateMessageCommand command,
        string expectedId)
        {
            // Arrange
            createMessageCommandHandlerMock.Setup(m => m.Handle(command)).Returns(expectedId);
            mapperMock.Setup(m => m.Map<CreateMessageCommand>(given)).Returns(command);

            var sut = MakeSut(
                mapper: mapperMock.Object,
                createMessageCommandHandler: createMessageCommandHandlerMock.Object);

            // Act
            var actual = sut.Create(given);

            // Assert
            actual.Should().BeOfType<CreatedResult>();
            (actual as CreatedResult).Value.Should().BeEquivalentTo(new { id = expectedId });
            (actual as CreatedResult).Location.Should().BeEmpty();

            createMessageCommandHandlerMock.VerifyAll();
            mapperMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void GetByConversationId(
            [Frozen] Mock<IMapper> mapperMock,
            [Frozen] Mock<IGetMessagesQueryHandler> getMessagesQueryHandlerMock,
            string given,
            IReadOnlyCollection<MessageDto> messagesDto,
            IEnumerable<Message> expected
        )
        {
            // Arrange
            mapperMock
                .Setup(m => m.Map<IEnumerable<Message>>(messagesDto))
                .Returns(expected);
            getMessagesQueryHandlerMock
                .Setup(m => m.Handle(It.Is<GetMessagesQuery>(query => query.ConversationId == given)))
                .Returns(messagesDto);

            var sut = MakeSut(mapper: mapperMock.Object, getMessagesQueryHandler: getMessagesQueryHandlerMock.Object);

            // Act
            var actual = sut.GetByConversationId(given);

            // Assert
            actual.Should().BeEquivalentTo(expected);
            mapperMock.VerifyAll();
            getMessagesQueryHandlerMock.VerifyAll();

        }
    }
}