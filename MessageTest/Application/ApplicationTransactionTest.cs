using System.Collections.Generic;
using MessageService.Application;
using MessageService.Domain.Api.Events;
using MessageService.Domain.Spi;
using Moq;
using Xunit;

namespace MessageTest.Application
{
    public class ApplicationTransactionTest
    {
        private ApplicationTransaction MakeSut(IDomainEventsContainer domainEventsContainer, IEnumerable<IDomainEventHandler> domainEventsHandlers)
        {
            return new ApplicationTransaction(domainEventsContainer, domainEventsHandlers);
        }

        [Fact]
        public void Commit_WhenHandlersHandles_It_ShouldCallHandle()
        {
            // Arrange
            var domainEventHandlerMock = new Mock<IDomainEventHandler>(MockBehavior.Strict);
            var domainEventMock = new Mock<IDomainEvent>(MockBehavior.Strict);
            var domainEventsContainerMock = new Mock<IDomainEventsContainer>(MockBehavior.Strict);

            var sut = MakeSut(domainEventsContainerMock.Object, new[] { domainEventHandlerMock.Object });

            domainEventsContainerMock.Setup(o => o.DomainEvents).Returns(new[] { domainEventMock.Object });
            domainEventsContainerMock.Setup(o => o.Clear());
            domainEventHandlerMock.Setup(o => o.Handles(domainEventMock.Object)).Returns(true);
            domainEventHandlerMock.Setup(o => o.Handle(domainEventMock.Object));

            // Act
            sut.Commit();

            // Assert
            domainEventsContainerMock.VerifyAll();
            domainEventHandlerMock.VerifyAll();
        }

        [Fact]
        public void Commit_WhenNoHandlersHandles_It_ShouldNotCallHandle()
        {
            // Arrange
            var domainEventHandlerMock = new Mock<IDomainEventHandler>(MockBehavior.Strict);
            var domainEventMock = new Mock<IDomainEvent>(MockBehavior.Strict);
            var domainEventsContainerMock = new Mock<IDomainEventsContainer>(MockBehavior.Strict);

            var sut = MakeSut(domainEventsContainerMock.Object, new[] { domainEventHandlerMock.Object });

            domainEventsContainerMock.Setup(o => o.DomainEvents).Returns(new[] { domainEventMock.Object });
            domainEventsContainerMock.Setup(o => o.Clear());
            domainEventHandlerMock.Setup(o => o.Handles(domainEventMock.Object)).Returns(false);

            // Act
            sut.Commit();

            // Assert
            domainEventsContainerMock.VerifyAll();
            domainEventHandlerMock.VerifyAll();
        }
    }
}