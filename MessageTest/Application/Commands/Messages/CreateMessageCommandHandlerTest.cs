using System;
using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using MessageService.Application;
using MessageService.Application.Commands.Messages;
using MessageService.Domain.Aggregates.Messages;
using MessageService.Domain.Aggregates.Messages.Events;
using MessageService.Domain.Api;
using MessageService.Domain.Api.Events;
using Moq;
using Xunit;

namespace MessageTest.Application.Commands.Messages
{
    public class CreateMessageCommandHandlerTest
    {
        [Theory, AutoMoq]
        public void Handle_Should_Send_Message(
            [Frozen] Mock<IDomainEventsContainer> domainEventsContainerMock,
            [Frozen] Mock<IMapper> mapperMock,
            [Frozen] Mock<IMessageFactory> messageFactoryMock,
            [Frozen] Mock<IApplicationTransaction> applicationTransactionMock,
            [Frozen] Mock<IDateTimeProvider> dateTimeProviderMock,

            DateTime expectedDateTime,
            Message expectedMessage,
            CreateMessageCommand given,
            CreateMessageCommandHandler sut
        )
        {
            // Setup
            messageFactoryMock.Setup(m => m.Create()).Returns(expectedMessage);
            mapperMock.Setup(m => m.Map(given, expectedMessage));
            applicationTransactionMock.Setup(m => m.Commit());
            dateTimeProviderMock.Setup(m => m.Now()).Returns(expectedDateTime);
            domainEventsContainerMock.Setup(m => m.Register(It.Is<IDomainEvent>(domainEvent =>
                domainEvent is MessageSentEvent &&
                (domainEvent as MessageSentEvent).Message == expectedMessage
            )));

            // Act
            var actual = sut.Handle(given);

            // Assert
            actual.Should().Be(expectedMessage.Id);
            expectedMessage.Date.Should().Be(expectedDateTime);

            messageFactoryMock.VerifyAll();
            mapperMock.VerifyAll();
            applicationTransactionMock.VerifyAll();
            dateTimeProviderMock.VerifyAll();
            domainEventsContainerMock.VerifyAll();
        }
    }
}