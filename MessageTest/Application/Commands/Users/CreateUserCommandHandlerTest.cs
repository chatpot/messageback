using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using MessageService.Application;
using MessageService.Application.Commands.Users;
using MessageService.Domain.Aggregates.Users;
using MessageService.Helpers;
using Moq;
using Xunit;

namespace MessageTest.Application.Commands.Users
{
    public class CreateUserCommandHandlerTest
    {
        [Theory, AutoMoq]
        public void Handle_When_UserDoesNotExist_ShouldCreateUser(
            [Frozen] Mock<IApplicationTransaction> applicationTransactionMock,
            [Frozen] Mock<IUserFactory> userFactoryMock,
            [Frozen] Mock<IUserRepository> userRepositoryMock,
            [Frozen] Mock<IMapper> mapperMock,

            User expectedUser,
            CreateUserCommand given,
            CreateUserCommandHandler sut)
        {
            // Arrange
            userRepositoryMock.Setup(m => m.FindOrDont(given.Id)).Returns(new Maybe<User>(null));
            userFactoryMock.Setup(m => m.Create(given.Id)).Returns(expectedUser);
            mapperMock.Setup(m => m.Map(given, expectedUser));
            applicationTransactionMock.Setup(m => m.Commit());

            // Act
            var actual = sut.Handle(given);

            // Assert
            actual.Should().Be(expectedUser.Id);

            userRepositoryMock.VerifyAll();
            userFactoryMock.VerifyAll();
            mapperMock.VerifyAll();
            applicationTransactionMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void Handle_When_UserExists_ShouldThrowException(
            [Frozen] Mock<IApplicationTransaction> applicationTransactionMock,
            [Frozen] Mock<IUserFactory> userFactoryMock,
            [Frozen] Mock<IUserRepository> userRepositoryMock,
            [Frozen] Mock<IMapper> mapperMock,

            User existingUser,
            CreateUserCommand given,
            CreateUserCommandHandler sut)
        {
            // Arrange
            userRepositoryMock.Setup(m => m.FindOrDont(given.Id)).Returns(new Maybe<User>(existingUser));

            // Act & Assert
            sut.Invoking(s => s.Handle(given)).Should().ThrowExactly<UserAlreadyRegisteredException>();

            userFactoryMock.Verify(m => m.Create(given.Id), Times.Never);
            mapperMock.Verify(m => m.Map(given, It.IsAny<User>()), Times.Never);
            applicationTransactionMock.Verify(m => m.Commit(), Times.Never);
        }
    }
}