using System;
using AutoFixture.Xunit2;
using FluentAssertions;
using MessageService.Application;
using MessageService.Application.Commands.Users;
using MessageService.Domain.Aggregates.Users;
using MessageService.Domain.Aggregates.Users.Events;
using MessageService.Domain.Api.Events;
using Moq;
using Xunit;

namespace MessageTest.Application.Commands.Users
{
    public class UpdateUserCommandHandlerTest
    {
        [Theory, AutoMoq]
        public void Handle_WhenUserExists_ShouldUpdateIt(
            [Frozen] Mock<IDomainEventsContainer> domainEventsContainerMock,
            [Frozen] Mock<IApplicationTransaction> applicationTransactionMock,
            [Frozen] Mock<IUserRepository> userRepositoryMock,
            User expectedUser,
            UpdateUserCommand given,
            UpdateUserCommandHandler sut)
        {
            // Arrange
            userRepositoryMock.Setup(m => m.Find(given.Id)).Returns(expectedUser);
            applicationTransactionMock.Setup(m => m.Commit());
            domainEventsContainerMock.Setup(m => m.Register(It.Is<IDomainEvent>(domainEvent =>
                domainEvent is UserUpdatedEvent &&
                (domainEvent as UserUpdatedEvent).User == expectedUser)));

            // Act
            var actual = sut.Handle(given);

            // Assert
            actual.Should().Be(expectedUser.Id);
            expectedUser.DisplayName.Should().Be(given.DisplayName);
            expectedUser.Image.Should().Be(given.Image);

            userRepositoryMock.VerifyAll();
            applicationTransactionMock.VerifyAll();
            domainEventsContainerMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void Handle_WhenUserDoesNotExist_ShouldThrow(
            [Frozen] Mock<IDomainEventsContainer> domainEventsContainerMock,
            [Frozen] Mock<IApplicationTransaction> applicationTransactionMock,
            [Frozen] Mock<IUserRepository> userRepositoryMock,
            User expectedUser,
            UpdateUserCommand given,
            UpdateUserCommandHandler sut)
        {
            // Arrange
            // TODO: I know System.Exception is very generic and does not allow one to generate proper response code
            userRepositoryMock.Setup(m => m.Find(given.Id)).Throws(new Exception());

            // Act && Assert
            sut.Invoking(s => s.Handle(given)).Should().Throw<Exception>();

            userRepositoryMock.VerifyAll();
            applicationTransactionMock.Verify(m => m.Commit(), Times.Never);
            domainEventsContainerMock.Verify(m => m.Register(It.IsAny<IDomainEvent>()), Times.Never);
        }
    }
}