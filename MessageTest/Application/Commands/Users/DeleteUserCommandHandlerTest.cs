using System;
using AutoFixture.Xunit2;
using FluentAssertions;
using MessageService.Application;
using MessageService.Application.Commands.Users;
using MessageService.Domain.Aggregates.Users;
using MessageService.Domain.Aggregates.Users.Events;
using MessageService.Domain.Api.Events;
using Moq;
using Xunit;

namespace MessageTest.Application.Commands.Users
{
    public class DeleteUserCommandHandlerTest
    {
        [Theory, AutoMoq]
        public void Handle_When_UserExists_Should_DeleteUser(
            [Frozen] Mock<IDomainEventsContainer> domainEventsContainerMock,
            [Frozen] Mock<IApplicationTransaction> applicationTransactionMock,
            [Frozen] Mock<IUserRepository> userRepositoryMock,
            User expectedUser,
            string given,
            DeleteUserCommandHandler sut)
        {
            // Arrange
            applicationTransactionMock.Setup(m => m.Commit());
            userRepositoryMock.Setup(m => m.Find(given)).Returns(expectedUser);
            domainEventsContainerMock.Setup(m => m.Register(
                It.Is<IDomainEvent>(domainEvent =>
                    domainEvent is UserDeletedEvent &&
                    (domainEvent as UserDeletedEvent).User == expectedUser)));

            // Act
            sut.Handle(given);

            // Assert
            domainEventsContainerMock.VerifyAll();
            applicationTransactionMock.VerifyAll();
            userRepositoryMock.VerifyAll();
        }

        [Theory, AutoMoq]
        public void Handle_When_UserDoesNotExist_Should_Throw(
            [Frozen] Mock<IDomainEventsContainer> domainEventsContainerMock,
            [Frozen] Mock<IApplicationTransaction> applicationTransactionMock,
            [Frozen] Mock<IUserRepository> userRepositoryMock,
            User expectedUser,
            string given,
            DeleteUserCommandHandler sut)
        {
            // Arrange
            // TODO: I know System.Exception is very generic and does not allow one to generate proper response code
            userRepositoryMock.Setup(m => m.Find(given)).Throws(new Exception());

            // Act & Assert
            sut.Invoking(s => s.Handle(given))
                .Should()
                .Throw<Exception>();

            userRepositoryMock.VerifyAll();
            applicationTransactionMock.Verify(m => m.Commit(), Times.Never);
            domainEventsContainerMock.Verify(m => m.Register(It.IsAny<IDomainEvent>()), Times.Never);
        }
    }
}