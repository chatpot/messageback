using System.Linq;
using AutoFixture.Xunit2;
using FluentAssertions;
using MessageService.Application;
using MessageService.Application.Commands.Conversations;
using MessageService.Domain.Aggregates.Conversations;
using MessageService.Domain.Aggregates.Conversations.Events;
using MessageService.Domain.Api.Events;
using Moq;
using Xunit;

namespace MessageTest.Application.Commands.Conversations
{
    public class UpdateConversationCommandHandlerTest
    {
        [Theory, AutoMoq]
        public void Handle_When_Members_Should_UpdateExistingConversation(
            [Frozen] Mock<IConversationRepository> conversationRepositoryMock,
            [Frozen] Mock<IDomainEventsContainer> domainEventsContainerMock,
            [Frozen] Mock<IApplicationTransaction> applicationTransactionMock,
            Conversation expectedConversation,
            UpdateConversationCommand given,
            UpdateConversationCommandHandler sut
        )
        {
            // Arrange 
            conversationRepositoryMock.Setup(m => m.Find(given.Id)).Returns(expectedConversation);
            applicationTransactionMock.Setup(m => m.Commit());
            domainEventsContainerMock.Setup(m => m.Register(
                It.Is<IDomainEvent>(domainEvent =>
                    domainEvent is ConversationUpdatedEvent &&
                    (domainEvent as ConversationUpdatedEvent).Conversation == expectedConversation)));

            // Act
            sut.Handle(given);

            // Assert
            conversationRepositoryMock.VerifyAll();
            applicationTransactionMock.VerifyAll();
            domainEventsContainerMock.VerifyAll();

            expectedConversation.Image.Should().Be(given.Image);
            expectedConversation.Name.Should().Be(given.Name);
            expectedConversation.Members.Should().BeEquivalentTo(given.Members);
        }

        [Theory, AutoMoq]
        public void Handle_When_NoMembers_Should_DeleteExistingConversation(
            [Frozen] Mock<IConversationRepository> conversationRepositoryMock,
            [Frozen] Mock<IDomainEventsContainer> domainEventsContainerMock,
            [Frozen] Mock<IApplicationTransaction> applicationTransactionMock,
            Conversation expectedConversation,
            UpdateConversationCommand given,
            UpdateConversationCommandHandler sut
        )
        {
            // Arrange
            given.Members = Enumerable.Empty<string>();
            conversationRepositoryMock.Setup(m => m.Find(given.Id)).Returns(expectedConversation);
            applicationTransactionMock.Setup(m => m.Commit());
            domainEventsContainerMock.Setup(m => m.Register(
                It.Is<IDomainEvent>(domainEvent =>
                    domainEvent is ConversationDeletedEvent &&
                    (domainEvent as ConversationDeletedEvent).Conversation == expectedConversation)));

            // Act
            sut.Handle(given);

            // Assert
            conversationRepositoryMock.VerifyAll();
            applicationTransactionMock.VerifyAll();
            domainEventsContainerMock.VerifyAll();

            expectedConversation.Image.Should().Be(given.Image);
            expectedConversation.Name.Should().Be(given.Name);
            expectedConversation.Members.Should().BeEquivalentTo(given.Members);
        }
    }
}