using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using MessageService.Application;
using MessageService.Application.Commands.Conversations;
using MessageService.Domain.Aggregates.Conversations;
using MessageService.Domain.Aggregates.Conversations.Events;
using MessageService.Domain.Api.Events;
using Moq;
using Xunit;

namespace MessageTest.Application.Commands.Conversations
{
    public class CreateConversationCommandHandlerTest
    {
        [Theory, AutoMoq]
        public void Handle_ShouldCreateANewConversation(
            [Frozen]Mock<IDomainEventsContainer> domainEventsContainerMock,
            [Frozen]Mock<IMapper> mapperMock,
            [Frozen]Mock<IConversationFactory> conversationFactoryMock,
            [Frozen]Mock<IApplicationTransaction> applicationTransactionMock,
            CreateConversationCommand given,
            Conversation expectedConversation,
            CreateConversationCommandHandler sut
        )
        {
            // Arrange
            conversationFactoryMock.Setup(m => m.Create()).Returns(expectedConversation);
            mapperMock.Setup(m => m.Map(given, expectedConversation));
            applicationTransactionMock.Setup(m => m.Commit());
            domainEventsContainerMock.Setup(m => m.Register(
                It.Is<IDomainEvent>(domainEvent =>
                    domainEvent is ConversationCreatedEvent &&
                    (domainEvent as ConversationCreatedEvent).Conversation == expectedConversation)));

            // Act
            var actual = sut.Handle(given);

            // Assert
            conversationFactoryMock.VerifyAll();
            mapperMock.VerifyAll();
            applicationTransactionMock.VerifyAll();
            domainEventsContainerMock.VerifyAll();

            actual.Should().Be(expectedConversation.Id);
        }
    }
}