using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using MessageService.Application.Queries.Users;
using MessageService.Domain.Aggregates.Users;
using Moq;
using Xunit;

namespace MessageTest.Application.Queries.Users
{
    public class GetUserQueryHandlerTest
    {
        [Theory, AutoMoq]
        public void Handle(
            User user,
            UserDto expected,
            [Frozen] Mock<IMapper> mapperMock,
            [Frozen] Mock<IUserRepository> userRepositoryMock,
            string input, GetUserQueryHandler sut)
        {
            // Arrange
            userRepositoryMock.Setup(o => o.Find(input)).Returns(user);
            mapperMock.Setup(o => o.Map<UserDto>(user)).Returns(expected);

            // Act
            var actual = sut.Handle(input);

            // Assert
            actual.Should().Be(expected);
        }
    }
}