using System.Collections.Generic;
using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using MessageService.Application.Queries.Users;
using MessageService.Domain.Aggregates.Users;
using Moq;
using Xunit;

namespace MessageTest.Application.Queries.Users
{
    public class GetUsersQueryHandlerTest
    {
        [Theory, AutoMoq]
        public void Handle(
            IReadOnlyCollection<User> users,
            IReadOnlyCollection<UserDto> expected,
            [Frozen] Mock<IMapper> mapperMock,
            [Frozen] Mock<IUserRepository> userRepositoryMock,
             GetUsersQueryHandler sut)
        {
            // Arrange
            userRepositoryMock.Setup(o => o.FindAll()).Returns(users);
            mapperMock.Setup(o => o.Map<IReadOnlyCollection<UserDto>>(users)).Returns(expected);

            // Act
            var actual = sut.Handle();

            // Assert
            actual.Should().BeSameAs(expected);
        }
    }
}