using System.Collections.Generic;
using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using MessageService.Application.Queries.Messages;
using MessageService.Domain.Aggregates.Messages;
using Moq;
using Xunit;

namespace MessageTest.Application.Queries.Messages
{
    public class GetMessagesQueryHandlerTest
    {
        [Theory, AutoMoq]
        public void Handle_WhenConversationId(
            IReadOnlyCollection<Message> messages,
            [Frozen] Mock<IMessageRepository> messageRepositoryMock,
            [Frozen] Mock<IMapper> mapperMock,
            IReadOnlyCollection<MessageDto> expected,
            GetMessagesQuery input,
            GetMessagesQueryHandler sut)
        {
            // Arrange
            messageRepositoryMock.Setup(o => o.FindByConversation(input.ConversationId)).Returns(messages);
            mapperMock.Setup(o => o.Map<IReadOnlyCollection<MessageDto>>(messages)).Returns(expected);

            // Act
            var actual = sut.Handle(input);

            // Assert
            actual.Should().NotBeEmpty();
            actual.Should().BeSameAs(expected);
        }

        [Theory, AutoMoq]
        public void Handle_WhenNoConversationId(
            IReadOnlyCollection<Message> messages,
            [Frozen] Mock<IMessageRepository> messageRepositoryMock,
            [Frozen] Mock<IMapper> mapperMock,
            IReadOnlyCollection<MessageDto> expected,
            GetMessagesQueryHandler sut)
        {
            // Arrange
            var input = new GetMessagesQuery(string.Empty);

            messageRepositoryMock.Setup(o => o.FindAll()).Returns(messages);
            mapperMock.Setup(o => o.Map<IReadOnlyCollection<MessageDto>>(messages)).Returns(expected);

            // Act
            var actual = sut.Handle(input);

            // Assert
            actual.Should().NotBeEmpty();
            actual.Should().BeSameAs(expected);
        }
    }
}