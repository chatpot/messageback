using System;
using System.Collections.Generic;
using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using MessageService.Application.Queries.Conversations;
using MessageService.Domain.Aggregates.Conversations;
using Moq;
using Xunit;

namespace MessageTest.Application.Queries.Conversations
{
    public class GetConversationsQueryHandlerTest
    {
        [Theory, AutoMoq]
        public void Handle_WhenNoMemberId_ShouldReturnAllConversations(
                IReadOnlyCollection<Conversation> conversations,
                IReadOnlyCollection<ConversationDto> expected,
                [Frozen]Mock<IConversationRepository> conversationRepositoryMock,
                [Frozen]Mock<IMapper> mapperMock,
                GetConversationsQueryHandler sut)
        {
            // Arrange
            var input = new GetConversationsQuery(string.Empty);

            conversationRepositoryMock.Setup(o => o.FindAll()).Returns(conversations);
            mapperMock.Setup(o => o.Map<IReadOnlyCollection<ConversationDto>>(conversations)).Returns(expected);

            // Act
            var actual = sut.Handle(input);

            // Assert
            actual.Should().NotBeEmpty();
            actual.Should().BeSameAs(expected);
        }

        [Theory, AutoMoq]
        public void Handle_WhenMemberId_ShouldReturnConversationsByMemberId(
            Guid inputId,
                IReadOnlyCollection<Conversation> conversations,
                IReadOnlyCollection<ConversationDto> expected,
                [Frozen]Mock<IConversationRepository> conversationRepositoryMock,
                [Frozen]Mock<IMapper> mapperMock,
                GetConversationsQueryHandler sut)
        {
            // Arrange
            var input = new GetConversationsQuery(inputId.ToString());

            conversationRepositoryMock.Setup(o => o.FindByMemberId(input.MemberId)).Returns(conversations);
            mapperMock.Setup(o => o.Map<IReadOnlyCollection<ConversationDto>>(conversations)).Returns(expected);

            // Act
            var actual = sut.Handle(input);

            // Assert
            actual.Should().NotBeEmpty();
            actual.Should().BeSameAs(expected);
        }
    }
}