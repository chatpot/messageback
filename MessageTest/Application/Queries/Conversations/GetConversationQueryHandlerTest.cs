using System;
using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using MessageService.Application.Queries.Conversations;
using MessageService.Domain.Aggregates.Conversations;
using Moq;
using Xunit;

namespace MessageTest.Application.Queries.Conversations
{
    public class GetConversationQueryHandlerTest
    {
        [Theory, AutoMoq]
        public void Handle(
            Guid input,
            Conversation conversation,
            ConversationDto expected,
            [Frozen]Mock<IConversationRepository> conversationRepositoryMock,
            [Frozen]Mock<IMapper> mapperMock,
            GetConversationQueryHandler sut)
        {
            // Arrange
            conversationRepositoryMock.Setup(o => o.Find(input.ToString())).Returns(conversation);
            mapperMock.Setup(o => o.Map<ConversationDto>(conversation)).Returns(expected);

            // Act
            var actual = sut.Handle(input.ToString());

            // Assert
            actual.Should().Be(expected);
        }
    }
}